<?php


namespace App\Services;


use App\Contact;
use Illuminate\Support\Facades\Mail;

class MailerService
{

    private $mailer;

    public function __construct()
    {
    }

    public function sendContactMail(Contact $contact)
    {
        try {

        Mail::send('mailer.contact',  ["contact" => $contact], function ($message) {
            $message->from('us@example.com', 'Demande de contact');
            $message->subject("Demande de contact");
            $message->to('us@example.com');
        });
        }catch (\Exception $e){
            dump($e->getMessage());
            return false;
        }

        return true;
    }

    public function sendContactNotification(Contact $contact)
    {

        try {
            Mail::send('mailer.notification', ["contact" => $contact], function ($message) use ($contact) {
                $message->from('us@example.com', 'Demande de contact');
                $message->subject("Demande de contact");
                $message->to($contact->email);
            });
        }catch (\Exception $e){
            dump($e->getMessage());
            return false;
        }

        return true;
    }
}

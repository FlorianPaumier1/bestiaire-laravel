<?php


namespace App\Services;


class Paginator
{

    /** @var int */
    private $length;

    /** @var array */
    private $pagination;

    public function __construct(int $length = 15)
    {
        $this->length = $length;
    }

    public function paginate(array $data, int $page = 1)
    {
        $offset = ($page - 1) * $this->length;

        $this->pagination = [
            "data" => array_slice($data, $offset, $this->length),
            "page" => $page,
            "count" => count($data),
            "page_count" => $this->length,
        ];

        return $this;
    }

    public function toJson(){
        return json_encode($this->pagination);
    }

    public function getArray()
    {
        return $this->pagination;
    }
}

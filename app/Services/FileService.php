<?php


namespace App\Services;


use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileService
{
    public static function store(UploadedFile $file, string $path = "/", bool $private = false){
        $fileName = md5($file->getClientOriginalName());
        $filePath = "/upload$path/";

        move_uploaded_file(
            $file->getPathname(),
            dirname(__DIR__, 2)."/public$filePath$fileName.".$file->getClientOriginalExtension());
//        $file->move( dirname(__DIR__, 2).$filePath,  $fileName.".".$file->getClientOriginalExtension());

        return $filePath . $fileName.".".$file->getClientOriginalExtension();
    }

    public static function exist(string $path)
    {
        return Storage::exists($path);
    }

    public function delete(string $path){
        return Storage::delete($path);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $fillable = ["name", 'close'];

    public function rooms()
    {
        return $this->hasMany(Room::class);
    }

    public function openRooms(int $length)
    {
        return $this->hasMany(Room::class)->where("close", false)->paginate($length);
    }

    public function creator(){
        return $this->hasOne(User::class);
    }
}

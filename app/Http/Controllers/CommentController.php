<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Room;
use App\Services\Paginator;
use App\Topic;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Request as HttpRequest;

class CommentController extends Controller
{
    public function index(Request $request,Topic $topic, Room $room){
        return $room->userComments()
            ->where('moderate', false)->paginate(15);
    }

    public function rank(HttpRequest $request,Topic $topic, Room $room, Comment $comment){
        $value = $request->request->get("value");

        if(!empty($value)){
            $comment->rate += $value;
            $comment->save();
            return ["message" => "Votre vote a été comptabilisé", "comment" => $comment];
        }

        return ["message" => "Votre vote n'a pa pu être comptabilisé", "comment" => $comment];
    }

    public function create(Request $request, Topic $topic, Room $room){

        $data = $request->validate([
            "comment" => 'required|min:10',
            "user" => "required"
        ]);

        /** @var User $user */
        $user = User::query()->find($data["user"]);

        if($user) {
            $comment = new Comment();
            $comment->comment = htmlspecialchars($data["comment"]);
            $comment->user()->associate($user);
            $comment->room()->associate($room);
            $comment->save();
        }

        $response = ["message" => "Votre commentaire à bien été ajouter", "comment" => $comment->load("user")];
        return json_encode($response);
    }
}

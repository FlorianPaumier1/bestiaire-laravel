<?php


namespace App\Http\Controllers;


use App\Comment;
use App\Models\Creature;
use App\Models\Espece;
use App\Models\Habitat;
use App\Models\Type;
use App\Room;
use App\Topic;
use App\Tutorial;
use App\TutorialChapter;
use App\Univer;
use Symfony\Component\HttpFoundation\Request;

class SearchController extends Controller
{

    public function search(Request $request)
    {
        $query = $request->query->get("q");


        $results = [
            "creatures" => Creature::query()
                ->whereRaw("LOWER(nom_creature) like ?" , ["%{$query}%"])
                ->orWhereRaw("LOWER(description_creature) like ?" , ["%{$query}%"])->get(),
            "univers" => Univer::query()->whereRaw("LOWER(nom_univers) like ?" , ["%{$query}%"])->get(),
            "habitats" => Habitat::query()->whereRaw("LOWER(nom_habitat) like ?" , ["%{$query}%"])->get(),
            "especes" => Espece::query()->whereRaw("LOWER(nom_espece) like ?" , ["%{$query}%"])->get(),
            "types" => Type::query()->whereRaw("LOWER(nom_type) like ?" , ["%{$query}%"])->get(),
            "rooms" => Room::query()->whereRaw("LOWER(title) like ?" , ["%{$query}%"])->get(),
            "comments" => Comment::query()->whereRaw("LOWER(comment) like ?" , ["%{$query}%"])->get(),
            "topics" => Topic::query()
                ->whereRaw("LOWER(name) like ?" , ["%{$query}%"])->get(),
            "tutorials" => Tutorial::query()
                ->whereRaw("LOWER(title) like ?" , ["%{$query}%"])
                ->orWhereRaw("LOWER(description) like ?" , ["%{$query}%"])->get(),
            "tutorials Chapter" => TutorialChapter::query()
                ->whereRaw("LOWER(content) like ?" , $query)
                ->orWhereRaw("LOWER(description) like ?", ["%{$query}%"])->get(),
        ];


        return view("search.search", ["results" => $results]);
    }
}

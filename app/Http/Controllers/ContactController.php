<?php


namespace App\Http\Controllers;

use App\Contact;
use App\Services\MailerService;
use Illuminate\Http\Request;

class ContactController extends Controller
{

    public function index()
    {
        return view("contact");
    }

    public function new(Request $request)
    {
        $data = $request->validate([
            "name" => "required",
            "email" => "required|email",
            "subject" => "required",
            "message" => "required|min:10",
        ]);

        $contact = new Contact();
        $contact->name = $data["name"];
        $contact->email = $data["email"];
        $contact->subject = $data["subject"];
        $contact->message = $data["message"];

        $contact->save();

        $mailer = new MailerService();

        $send = $mailer->sendContactMail($contact);

        if($send){
            $contact->send = true;
        }

        $contact->update();

        $mailer->sendContactNotification($contact);

        return redirect()->route("app_contact");
    }
}

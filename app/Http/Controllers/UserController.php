<?php


namespace App\Http\Controllers;


use App\User;
use Illuminate\Auth\Access\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    public function profile(User $user)
    {
        if(!$this->allowed($user)) return redirect()->route("home");
        return view("user.profile", ["user" => $user]);
    }

    public function paymentInfo(Request $request){
        $user = $request->user();
        if(!$this->allowed($user)) return redirect()->route("home");

        return view("user.payment_info", ["intent" => $user->createSetupIntent()]);
    }

    public function billingPortal(Request $request)
    {
        return $request->user()->redirectToBillingPortal();
    }

    public function showFormUpdate(User $user)
    {
        if(!$this->allowed($user)) return redirect()->route("home");
        return view("user.edit_profile", ["user" => $user]);
    }

    public function update(Request $request, User $user)
    {
        if(!$this->allowed($user)) return redirect()->route("home");
        $this->validateUser($request);

        $user->name = request('name');
        $user->email = request('email');

        if(!empty(request('password')))
            $user->password = bcrypt(request('password'));

        $user->save();

        return back();
    }

    public function destroy(Request $request, User $user)
    {
        if(!$this->allowed($user)) return redirect()->route("home");

        $user->delete();

        return redirect()->route('home')->with("success", "Votre compte à bien été supprimé. A bientôt");
    }

    /**
     * Validate the user login request.
     *
     * @param  Request  $request
     * @return void
     *
     */
    protected function validateUser(Request $request)
    {
        if(Auth::user()->email !== request('email')){
            $request->validate([
                'email' => 'required|email|unique:users',
            ]);
        }

        if(request('password')){
            $request->validate([
                'password' => 'string|min:8|confirmed',
            ]);
        }

        $request->validate([
            'name' => 'required|string',
        ]);
    }

    public function isConnected()
    {
        return ["connected" => !is_null(Auth::user()), "user" => !is_null(Auth::user()) ? Auth::user()->getAuthIdentifier() : null];
    }

    private function allowed(User $user){
        return \Illuminate\Support\Facades\Gate::allows("update-profile", $user);
    }
}

<?php

namespace App\Http\Controllers;

use App\Room;
use App\Topic;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoomController extends Controller
{
    public function index(Request $request, Topic $topic, Room $room)
    {
        if($topic->id != $room->topic()->value("id")){
            return redirect()->route("app.forum.topic", ["topic" => $topic]);
        }

        return view("forum.room.index", [
            "room" => $room,
        ]);
    }

    public function new(Request $request, Topic $topic){
        return view("forum.room.new", ["topic" => $topic->slug]);
    }

    public function create(Request $request, Topic $topic)
    {
        $data = $request->validate([
           "room_title" => 'required|min:10',
           "room_description" => 'required|min:50'
        ]);

        $room = new Room();
        $room->title = $data["room_title"];
        $room->message = htmlspecialchars($data["room_description"]);
        $room->user()->associate(Auth::user());
        $room->topic()->associate($topic);

        $room->save();

        return redirect()->route("app.forum.topic.room", ["topic" => $topic, "room" => $room]);
    }
}

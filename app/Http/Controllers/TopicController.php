<?php

namespace App\Http\Controllers;

use App\Services\Paginator;
use App\Topic;
use Illuminate\Http\Request;

class TopicController extends Controller
{
    public function index(Request $request, Topic $topic)
    {
        $paginator = new Paginator();

        return view("forum.topic.index", [
            "rooms" => $topic->openRooms(15),
            "slug" => $topic->slug,
        ]);
    }
}

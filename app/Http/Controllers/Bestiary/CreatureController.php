<?php

namespace App\Http\Controllers\Bestiary;

use App\Http\Controllers\Controller;

use App\Models\Creature;
use App\Models\CreatureType;
use App\Models\CreatureHabitat;
use App\Models\CreatureUniver;
use App\Models\Espece;
use App\Models\Habitat;
use App\Models\Type;
use App\Models\Univer;
use App\Services\FileService;
use Illuminate\Support\Facades\Log;
use Carbon\Traits\Creator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CreatureController extends Controller
{
    // méthodes en rapport avec le chargement des infos d'une créature /////////////////////////////////////////////////////////////
    private function getCreatureImage(Creature $creature)
    {
        $defaultImagePath = '/images/default-unknown-creature.png';
        $creatureImagePath = '/upload/creatures/' . $creature->id_creature . '.png';

        return file_exists(public_path($creatureImagePath))
            ? $creatureImagePath
            : $defaultImagePath;
    }

    private function loadCreatureProfile(Creature $creature)
    {
        return [
            'creature' => $creature,
            'especes' => $creature->espece()->get(),
            'habitats' => $creature->habitats()->get(),
            'types' => $creature->types()->get(),
            'univers' => $creature->univers()->get(),
        ];
    }

    public function creatureProfile(Creature $creature)
    {
        return view('bestiary.creature_profile', $this->loadCreatureProfile($creature));
    }



    // méthodes en rapport avec la modification d'une créature /////////////////////////////////////////////////////////////
    public function creatureUpdateForm(Creature $creature)
    {

        return view(
            'bestiary.creature_update_form',
            [
                'allEspeces' => Espece::all(),
                'allTypes' => Type::all(),
                'allHabitat' => Habitat::all(),
                'allUnivers' => Univer::all(),
                'creature' => $this->loadCreatureProfile($creature),
                'image' => $this->getCreatureImage($creature)
            ]
        );
    }

    public function creatureUpdate(Request $request, Creature $creature)
    {
        $request->validate([
            "nom_creature" => "required",
            "espece" => "required",
            "habitats" => "required",
            "univers" => "required",
            "types" => "required",
        ]);

        if(!is_null($request->file("creature_icon")))
            $creature->image = FileService::store($request->file("creature_icon"), '/creatures');


        $creature->nom_creature = $request->input('nom_creature');
        $creature->espece()->associate(Espece::find($request->input('espece')));
        $creature->habitats()->sync($request->input('habitats'));
        $creature->univers()->sync($request->input('univers'));
        $creature->types()->sync($request->input('types'));

        $creature->save();


        return redirect()->route('bestiary.creature_profile', ['creature' => $creature]);
    }



    // méthodes en rapport avec l'ajout d'une créature /////////////////////////////////////////////////////////////
    public function ViewAddCreature(Creature $creature)
    {
        return view(
            'bestiary.creature_add',
            [
                'allEspeces' => Espece::all(),
                'allTypes' => Type::all(),
                'allHabitat' => Habitat::all(),
                'allUnivers' => Univer::all(),
                'creature' => $this->loadCreatureProfile($creature),
            ]
        );
    }

    public function addCreatureElements(Request $request)
    {
        if ($request->input('addTypeSubmit') != null) {
            $addType = $request->input('typeName');
            $type_name = array('nom_type' => $addType);
            Type::insert($type_name);
        } elseif ($request->input('addHabitatSubmit') != null) {
            $addHabitat = $request->input('habitatName');
            $habitat_name = array('nom_habitat' => $addHabitat);
            Habitat::insert($habitat_name);
        } elseif ($request->input('addUniversSubmit') != null) {
            $addUnivers = $request->input('universName');
            $univers_name = array('nom_univers' => $addUnivers);
            Univer::insert($univers_name);
        } elseif ($request->input('addCreatureSubmit') != null) {
            return $this->addCreature($request);
        }
        return redirect()->route('bestiary.add_creature');
    }

    private function addCreature(Request $request)
    {
        $request->validate([
            "nom_creature" => "required",
            "espece" => "required",
            "habitats" => "",
            "univers" => "",
            "types" => "",
        ]);

        $creature = new Creature();
        if(!is_null($request->file("creature_icon")))
            $creature->image = FileService::store($request->file("creature_icon"), '/creatures');

        $creature->nom_creature =  $request->input('nom_creature');
        $creature->espece()->associate(Espece::find($request->input('espece')));
        $creature->habitats()->sync($request->input('habitats'));
        $creature->univers()->sync($request->input('univers'));
        $creature->types()->sync($request->input('types'));

        $creature->save();
        return redirect()->route('bestiary.creature_list');
    }

    // méthodes en rapport avec la liste des créatures /////////////////////////////////////////////////////////////
    public function viewCreaturesList()
    {
        return view('bestiary.creature_list', [
            'creatureList' => Creature::all()
            ->sortBy("nom_creature", SORT_NATURAL|SORT_FLAG_CASE)
            ->map(
                function ($creature) {
                    return [
                        'entity' => $creature
                    ];
                }
            ),
            'allEspeces' => Espece::all(),
            'allTypes' => Type::all(),
            'allHabitat' => Habitat::all(),
            'allUnivers' => Univer::all()
        ]);
    }

    public function filterCreature(Request $request)
    {
        $filteredCreatures = Creature::all();

        if ($request->input('especeSelect') != null) {
            $filteredCreatures = $filteredCreatures->filter(function ($creature) use ($request) {
                return $creature->espece()->get()
                    ->filter(function ($espece) use ($request) {
                        return data_get($espece, 'id_espece') == $request->especeSelect;
                    })
                    ->isNotEmpty();
            });
        }

        if ($request->input('typeSelect') != null) {
            $filteredCreatures = $filteredCreatures->filter(function ($creature) use ($request) {
                return $creature->types()->get()
                    ->filter(function ($type) use ($request) {
                        return data_get($type, 'id_type') == $request->typeSelect;
                    })
                    ->isNotEmpty();
            });
        }

        if ($request->input('habitatSelect') != null) {
            $filteredCreatures = $filteredCreatures->filter(function ($creature) use ($request) {
                return $creature->habitats()->get()
                    ->filter(function ($habitat) use ($request) {
                        return data_get($habitat, 'id_habitat') == $request->habitatSelect;
                    })
                    ->isNotEmpty();
            });
        }

        if ($request->input('universSelect') != null) {
            $filteredCreatures = $filteredCreatures->filter(function ($creature) use ($request) {
                return $creature->univers()->get()
                    ->filter(function ($univers) use ($request) {
                        return data_get($univers, 'id_univers') == $request->universSelect;
                    })
                    ->isNotEmpty();
            });
        }

        return view('bestiary.creature_list', [
            'creatureList' => $filteredCreatures
                ->sortBy('nom_creature', SORT_NATURAL|SORT_FLAG_CASE)
                ->map(function ($creature) {
                    return [
                        'entity' => $creature,
                        'image' => $this->getCreatureImage($creature)
                    ];
                }),
            'allEspeces' => Espece::all(),
            'allTypes' => Type::all(),
            'allHabitat' => Habitat::all(),
            'allUnivers' => Univer::all()
        ]);
    }

    // méthodes en rapport avec la suppression d'une créature /////////////////////////////////////////////////////////////

    public function ViewDeleteCreature(Creature $creature)
    {
        return view('bestiary.creature_delete', $this->loadCreatureProfile($creature));
    }

    public function destroyCreature(Creature $creature)
    {
        if (isset($_POST['defDeleteBtn'])) {
            Creature::where('id_creature', $creature['id_creature'])->delete();
            return redirect()->route('bestiary.creature_list');
        }
    }
}

<?php


namespace App\Http\Controllers;


use App\Tutorial;
use App\TutorialChapter;
use Symfony\Component\HttpFoundation\Request;

class TutorialController extends Controller
{

    public function index()
    {
        $tutorials = [];

        array_map(function($tuto) use (&$tutorials) {
            if(!key_exists($tuto["univer_id"], $tutorials) || count($tutorials[$tuto["univer_id"]]) < 6){
                $tutorials[$tuto["univer_id"]][] = $tuto;
            }
        }, Tutorial::all()->toArray());

        return view("tutorial.index", [
            "univers" => $tutorials
        ]);
    }

    public function show(Request $request,Tutorial $tutorial)
    {
        $chapter = null;
        if($request->get("chapter")){
            $chapter = TutorialChapter::query()->find($request->get("chapter"));
        }
        return view("tutorial.show", [
           "tutorial" => $tutorial,
            "chapter" => $chapter,
        ]);
    }
}

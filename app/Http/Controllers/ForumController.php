<?php


namespace App\Http\Controllers;


use App\Topic;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;

class ForumController extends Controller
{

    /**
     * Plusieurs Topic
     * plusieur room
     * plusieur comment
     */

    public function index(Request $request)
    {
        $topics = Topic::all()->where("close", false);
        return view("forum/index", [
            "topics" => $topics,
        ]);
    }
}

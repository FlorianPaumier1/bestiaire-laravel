<?php


namespace App\Http\Controllers\Admin;


use App\Comment;
use App\Http\Controllers\Controller;
use App\Room;
use App\Services\Slug;
use App\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CommentsController extends Controller
{

    public function index(Topic $topic, Room $room)
    {
        return view("admin.comment.index", [
            "topic" => $topic,
            "room" => $room,
            "comments" => $room->comments->simplePaginate(15)
        ]);
    }

    public function edit(Topic $topic, Room $room,Comment $comment){
        return view("admin.comment.edit", [
            "topic" => $topic,
            "room" => $room,
            "comment" => $comment,
        ]);
    }

    public function update(Request $request,Topic $topic, Room $room, Comment $comment){

        $data = $request->validate([
            "comment_content" => "required|max:255",
            "comment_moderate" => "",
            "comment_rate" => "required",
        ]);

        $comment->comment = $data["comment_content"];


        if (isset($data["comment_moderate"])) {
            $moderate = !($data["comment_moderate"] == "on");
            $comment->moderate = $moderate;
        }else{
            $comment->moderate = true;
        }

        $comment->rate = $data["comment_rate"];

        $comment->update();

        return redirect()->route("admin.comments.index", ["topic" => $topic, "room" => $room]);
    }

    public function delete(Comment $comment)
    {
        $comment->delete();
        return redirect()->route("admin.comment.index");
    }
}

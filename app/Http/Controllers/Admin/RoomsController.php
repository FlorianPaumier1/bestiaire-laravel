<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Room;
use App\Services\Slug;
use App\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RoomsController extends Controller
{

    public function index(Topic $topic)
    {

        return view("admin.room.index", [
            "topic" => $topic,
            "rooms" => $topic->rooms()->simplePaginate(15)
        ]);
    }

    public function edit(Topic $topic, Room $room)
    {
        return view("admin.room.edit", [
            "topic" => $topic,
            "room" => $room,
        ]);
    }

    public function update(Request $request, Topic $topic, Room $room)
    {
        $data = $request->validate([
            "room_title" => "required|max:255",
            "room_close" => "",
            "room_moderate" => "",
        ]);

        $room->title = $data["room_title"];

        if (isset($data["room_close"])) {
            $close = $data["room_close"] == "on";
            $room->close = $close;
        }else{
            $room->close = false;
        }

        if (isset($data["room_moderate"])) {
            $moderate = $data["room_moderate"] == "on";
            $room->moderate = $moderate;
        }else{
            $room->moderate = false;
        }

        $room->update();

        return redirect()->route("admin.rooms.index", ["topic" => $topic]);
    }

    public function delete(Topic $topic, Room $room)
    {
        $room->delete();
        return redirect()->route("admin.rooms.index", ["topic" => $topic]);
    }
}

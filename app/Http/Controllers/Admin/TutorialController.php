<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Univer;
use App\Services\FileService;
use App\Tutorial;
use Illuminate\Http\Request;

class TutorialController extends Controller
{

    public function index()
    {
        return view("admin.tutorial.index", [
            "tutorials" => Tutorial::query()->paginate(10),
        ]);
    }

    public function show(Tutorial $tutorial)
    {

        return view("admin.tutorial.show", [
            "tutorial" => $tutorial,
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            "title" => "required|unique:tutorials",
            "univer_id" => "required",
            "thumbnail" => "required"
        ]);

        $tutorial = new Tutorial();
        $tutorial->title = $request->get("title");
        $tutorial->univer_id = $request->get("univer_id");
        $tutorial->visible = $request->get("visible") ? true : false;
        $tutorial->thumbnail = FileService::store($request->file("thumbnail"), "tutorials/thumbnails");

        $tutorial->save();
        return redirect()->route("admin.tutorials.index");

    }

    public function create()
    {

        $univers = [];

        foreach (Univer::all()->toArray() as $univer) {
            $univers[$univer["id"]] = $univer["nom_univers"];
        }
        $tutorial = new Tutorial();

        return view("admin.tutorial.new", [
            "univers" => $univers,
            "tutorial" => $tutorial,
        ]);
    }

    public function edit(Tutorial $tutorial)
    {
        $univers = [];

        foreach (Univer::all()->toArray() as $univer) {
            $univers[$univer["id_univers"]] = $univer["nom_univers"];
        }
        return view("admin.tutorial.edit", [
            "univers" => $univers,
            "tutorial" => $tutorial,
        ]);
    }

    public function update(Request $request, Tutorial $tutorial)
    {
        $request->validate([
            "title" => "required:tutorials",
            "univer_id" => "required",
        ]);

        $title = Tutorial::query()
            ->where("title", "=",  $request->get("title"))
            ->where("id", "!=", $tutorial->id)->get();

        if(count($title) > 0){
            $request->session()->flash('error', 'Title need to be unique');
            return redirect()->route("admin.tutorials.edit", ["tutorial" => $tutorial->id]);
        }

        $tutorial->title = $request->get("title");

        $tutorial->univer_id = $request->get("univer_id");

        $tutorial->visible = $request->get("visible") ? true : false;

        if($request->file("thumbnail")){
            $tutorial->thumbnail = FileService::store($request->file("thumbnail"), "tutorials/thumbnails");
        }

        $tutorial->update();
        return redirect()->route("admin.tutorials.index");
    }

    public function destroy(Tutorial $tutorial)
    {
        $tutorial->delete();
        return redirect()->route("admin.tutorials.index");
    }
}

<?php


namespace App\Http\Controllers\Admin;


use App\Contact;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{

    public function index()
    {
        return view("admin.contact.index", [
            "contacts" => Contact::query()->paginate(15)
        ]);
    }

    public function update(Contact $contact){

        $contact->closed = true;
        $contact->update();

        return redirect()->route("admin.contact.index");
    }

    public function delete(Contact $contact){
        $contact->delete();
        return redirect()->route("admin.contact.index");
    }


}

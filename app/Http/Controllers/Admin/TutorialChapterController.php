<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Tutorial;
use App\TutorialChapter;
use Illuminate\Http\Request;

class TutorialChapterController extends Controller
{

    public function index(Tutorial $tutorial)
    {
        return view("admin.tutorial_chapters.index", [
            "tutorial" => $tutorial,
            "chapters" => TutorialChapter::query()->where("tutorial_id", "=", $tutorial->id)->paginate(10),
        ]);
    }

    public function show(Tutorial $tutorial, TutorialChapter $tutorialChapter)
    {
        return view("admin.tutorial_chapters.show", [
            "chapter" =>  $tutorialChapter,
        ]);
    }

    public function store(Request $request, Tutorial $tutorial)
    {
        $request->validate([
            "title" => "required|max:255",
            "position" => "required|min:1",
            "description" => "required|max:1000",
            "content" => "required"
        ]);

        $chapter = new TutorialChapter();

        $chapter->title = $request->request->get("title");
        $chapter->position = $request->request->get("position");
        $chapter->description = $request->request->get("description");
        $chapter->content = $request->request->get("content");
        $chapter->tutorial_id = $tutorial->id;

        $chapter->save();
        return redirect()->route("admin.tutorial_chapter.index", ["tutorial" => $tutorial]);
    }

    public function create(Tutorial $tutorial){
        return view("admin.tutorial_chapters.new", [
            "tutorial" => $tutorial
        ]);
    }

    public function edit(Tutorial $tutorial, TutorialChapter $tutorialChapter){

        return view("admin.tutorial_chapters.edit", [
            "tutorial" => $tutorial,
            "chapter" => $tutorialChapter,
        ]);
    }

    public function update(Request $request, Tutorial $tutorial,  TutorialChapter $tutorialChapter){
        $request->validate([
            "title" => "required|max:255",
            "position" => "required|min:1",
            "description" => "required|max:1000",
            "content" => "required"
        ]);

        $tutorialChapter->title = $request->request->get("title");
        $tutorialChapter->position = $request->request->get("position");
        $tutorialChapter->description = $request->request->get("description");
        $tutorialChapter->content = $request->request->get("content");
        $tutorialChapter->tutorial_id = $tutorial->id;

        $tutorialChapter->update();
        return redirect()->route("admin.tutorial_chapter.index", ["tutorial" => $tutorial]);
    }

    public function delete(Tutorial $tutorial,  TutorialChapter $tutorialChapter){
        $tutorialChapter->delete();
        return redirect()->route("admin.tutorial_chapter.index", ["tutorial" => $tutorial]);
    }
}

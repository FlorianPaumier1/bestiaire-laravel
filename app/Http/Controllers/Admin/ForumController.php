<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Services\Slug;
use App\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ForumController extends Controller
{

    public function index()
    {
        return view("admin.forum.index", [
            "topics" => Topic::query()->simplePaginate(15)
        ]);
    }

    public function edit(Topic $topic){
        return view("admin.forum.edit", [
            "topic" => $topic,
        ]);
    }

    public function update(Request $request, Topic $topic){
        $data = $request->validate([
            "topic_name" => "required|max:255",
            "topic_close" => "required"
        ]);

        $close = $data["topic_close"] == "on";

        $topic->name = $data["topic_name"];
        $topic->close = $close;
        $topic->slug = Str::of($topic->name)->slug('-');;

        $topic->update();

        return redirect()->route("admin.forum.index");
    }

    public function delete(Topic $topic)
    {
        $topic->delete();
        return redirect()->route("admin.forum.index");
    }
}

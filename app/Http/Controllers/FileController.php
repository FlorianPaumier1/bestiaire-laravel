<?php


namespace App\Http\Controllers;


use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

class FileController extends Controller
{

    public function displayImage($filename)
    {
        $path = storage_public('storage/' . $filename);

        if (!File::exists($path)) {
            abort(404);
        }

        $response = Response::make(File::get($path), 200);
        $response->header("Content-Type", File::mimeType($path));

        dump($response);
        return $response;

    }
}

<?php

namespace App;

use App\Models\Creature;
use App\Models\Espece;
use App\Models\Habitat;
use App\Models\Type;
use Illuminate\Database\Eloquent\Model;

class TutorialChapter extends Model
{

    public function description()
    {
        return $this->parseView($this->description);
    }


    public function content()
    {
        return $this->parseView($this->content);
    }

    public function tutorial()
    {
        return $this->belongsTo(Tutorial::class);
    }

    private function parseView(string $content): string
    {
        preg_match_all("#\s?\[(.*?)=(.*?)\]\s?#m", $content, $matches, PREG_SET_ORDER);

        foreach ($matches as $match) {
            $url = "";

            switch ($match[1]) {
                case 'univers':
                    $univer = Univer::query()->firstWhere("nom_univers", "=", $match[2]);
                    if($univer)
                        $url = route("app.univers.show", ["univers" => $univer->id_univers]);
                    break;
                case 'monsters':
                    $monster = Creature::query()->firstWhere("nom_creature", "=", $match[2]);
                    if($monster)
                        $url = route("bestiary.creature_profile", ["creature" => $monster->id_creature]);
                    break;
                case 'species':
                    $specie = Espece::query()->firstWhere("nom_espece", "=", $match[2]);
                    if($specie)
                        $url = route("app.species.show", ["spieces" => $specie->id_escpece]);
                    break;
                case 'places':
                    $place = Habitat::query()->firstWhere("nom_habitat", "=", $match[2]);
                    if($place)
                        $url = route("app.places.show", ["places" => $place->id_habitat]);
                    break;
                case 'types':
                    $type = Type::query()->firstWhere("nom_type", "=", $match[2]);
                    if($type)
                        $url = route("app.types.show", ["types" => $type->id_type]);
                    break;
            }

            if ($url) {
                $content = preg_replace("#\s?\[" . $match[1] . "=" . $match[2] . "\]\s?#", "<a href='$url'>" . $match[2] . "</a>", $content);
            }else{
                $content = preg_replace("#\s?\[" . $match[1] . "=" . $match[2] . "\]\s?#", "", $content);
            }
        }

        return $content;
    }
}

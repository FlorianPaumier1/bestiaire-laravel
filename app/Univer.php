<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Univer extends Model
{

    protected $fillable = [
        'nom_univers',
    ];

    public function list(){
        $array = [];

        foreach ($this->toArray() as $univer){
            $array[$univer["id"]] = $univer["nom_univers"];
        }

        return $array;
    }
}

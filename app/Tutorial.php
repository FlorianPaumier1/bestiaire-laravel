<?php

namespace App;

use App\Models\Univer;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Tutorial extends
    Model
{

    use Sluggable;

    protected $fillable = ["title", "visible", "thumbnail", "slug"];

    public function univers()
    {
        return $this->belongsTo(Univer::class, 'univer_id', 'id_univers');
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function tutorialChapters()
    {
        return $this->hasMany(TutorialChapter::class);
    }
}

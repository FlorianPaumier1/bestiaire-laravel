<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = ["title", 'close', 'moderate'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
    }

    public function comments()
    {
        return $this->hasMany(Comment::class)->orderBy('comments.created_at', 'ASC');
    }

    public function userComments()
    {
        return $this->hasMany(Comment::class)->orderBy('comments.created_at', 'ASC')
            ->select(["comments.*", "users.email", "users.name"])
            ->join('users', 'users.id', '=', 'comments.user_id');
    }

    public function topic(){
        return $this->belongsTo(Topic::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

}

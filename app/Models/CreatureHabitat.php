<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CreatureHabitat
 * 
 * @property int $id_creature
 * @property int $id_habitat
 * 
 * @property Creature $creature
 * @property Habitat $habitat
 *
 * @package App\Models
 */
class CreatureHabitat extends Model
{
	protected $table = 'creature_habitat';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id_creature' => 'int',
		'id_habitat' => 'int'
	];

	public function creature()
	{
		return $this->belongsTo(Creature::class, 'id_creature');
	}

	public function habitat()
	{
		return $this->belongsTo(Habitat::class, 'id_habitat');
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CreatureUniver
 * 
 * @property int $id_creature
 * @property int $id_univers
 * 
 * @property Creature $creature
 * @property Univer $univer
 *
 * @package App\Models
 */
class CreatureUniver extends Model
{
	protected $table = 'creature_univers';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id_creature' => 'int',
		'id_univers' => 'int'
	];

	public function creature()
	{
		return $this->belongsTo(Creature::class, 'id_creature');
	}

	public function univer()
	{
		return $this->belongsTo(Univer::class, 'id_univers');
	}
}

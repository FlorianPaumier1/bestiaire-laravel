<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CreatureType
 * 
 * @property int $id_creature
 * @property int $id_type
 * 
 * @property Type $type
 * @property Creature $creature
 *
 * @package App\Models
 */
class CreatureType extends Model
{
	protected $table = 'creature_type';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id_creature' => 'int',
		'id_type' => 'int'
	];

	protected $fillable = [
		'id_creature',
		'id_type'
	];

	public function type()
	{
		return $this->belongsTo(Type::class, 'id_type');
	}

	public function creature()
	{
		return $this->belongsTo(Creature::class, 'id_creature');
	}
}

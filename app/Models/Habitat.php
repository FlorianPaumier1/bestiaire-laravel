<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Habitat
 * 
 * @property int $id_habitat
 * @property string $nom_habitat
 * 
 * @property Collection|Creature[] $creatures
 *
 * @package App\Models
 */
class Habitat extends Model
{
	protected $table = 'habitat';
	protected $primaryKey = 'id_habitat';
	public $timestamps = false;

	protected $fillable = [
		'nom_habitat'
	];

	public function creatures()
	{
		return $this->belongsToMany(Creature::class, 'creature_habitat', 'id_habitat', 'id_creature');
	}
}

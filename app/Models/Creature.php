<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Creature
 * 
 * @property int $id_creature
 * @property string|null $nom_creature
 * @property int|null $id_espece
 * @property string|null $description_creature
 * 
 * @property Espece $espece
 * @property Collection|Habitat[] $habitats
 * @property Collection|Type[] $types
 * @property Collection|Univer[] $univers
 *
 * @package App\Models
 */
class Creature extends Model
{
	protected $table = 'creatures';
	protected $primaryKey = 'id_creature';
	public $timestamps = false;

	protected $casts = [
		'id_espece' => 'int'
	];

	protected $fillable = [
		'nom_creature',
		'id_espece',
		'description_creature'
	];

	public function espece()
	{
		return $this->belongsTo(Espece::class, 'id_espece');
	}

	public function habitats()
	{
		return $this->belongsToMany(Habitat::class, 'creature_habitat', 'id_creature', 'id_habitat');
	}

	public function types()
	{
		return $this->belongsToMany(Type::class, 'creature_type', 'id_creature', 'id_type');
	}

	public function univers()
	{
		return $this->belongsToMany(Univer::class, 'creature_univers', 'id_creature', 'id_univers');
	}
}

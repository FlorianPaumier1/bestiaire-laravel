<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Univer
 * 
 * @property int $id_univers
 * @property string $nom_univers
 * @property string $image_univers
 * 
 * @property Collection|Creature[] $creatures
 *
 * @package App\Models
 */
class Univer extends Model
{
	protected $table = 'univers';
	protected $primaryKey = 'id_univers';
	public $timestamps = false;

	protected $fillable = [
		'nom_univers',
		'image_univers'
	];

	public function creatures()
	{
		return $this->belongsToMany(Creature::class, 'creature_univers', 'id_univers', 'id_creature');
	}
}

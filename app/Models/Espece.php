<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Espece
 * 
 * @property int $id_espece
 * @property string $nom_espece
 * 
 * @property Collection|Creature[] $creatures
 *
 * @package App\Models
 */
class Espece extends Model
{
	protected $table = 'espece';
	protected $primaryKey = 'id_espece';
	public $timestamps = false;

	protected $fillable = [
		'nom_espece'
	];

	public function creatures()
	{
		return $this->hasMany(Creature::class, 'id_espece');
	}
}

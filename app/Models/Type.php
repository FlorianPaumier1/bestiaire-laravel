<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Type
 * 
 * @property int $id_type
 * @property string $nom_type
 * 
 * @property Collection|Creature[] $creatures
 *
 * @package App\Models
 */
class Type extends Model
{
	protected $table = 'type';
	protected $primaryKey = 'id_type';
	public $timestamps = false;

	protected $fillable = [
		'id_type',
		'nom_type'
	];

	public function creatures()
	{
		return $this->belongsToMany(Creature::class, 'creature_type', 'id_type', 'id_creature');
	}
}

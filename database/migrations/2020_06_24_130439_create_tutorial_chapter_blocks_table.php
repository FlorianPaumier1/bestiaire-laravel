<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTutorialChapterBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutorial_chapter_blocks', function (Blueprint $table) {
            $table->id();
            $table->string("title")->nullable();
            $table->longText("content");
            $table->integer("position")->unsigned()->default(1);
            $table->integer("tutorial_chapter_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutorial_chapter_blocks');
    }
}

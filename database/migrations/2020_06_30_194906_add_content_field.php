<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddContentField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tutorial_chapters', function (Blueprint $table) {
            $table->longText("content");
        });

        Schema::drop("tutorial_chapter_blocks");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tutorial_chapters', function (Blueprint $table) {
            $table->dropColumn("content");
        });
    }
}

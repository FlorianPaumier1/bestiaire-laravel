-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 14 juin 2020 à 14:41
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bestiaire`
--

-- --------------------------------------------------------

--
-- Structure de la table `creatures`
--

DROP TABLE IF EXISTS `creatures`;
CREATE TABLE IF NOT EXISTS `creatures` (
  `id_creature` int(11) NOT NULL AUTO_INCREMENT,
  `nom_creature` text COLLATE utf8_bin NOT NULL,
  `image_creature` text COLLATE utf8_bin NOT NULL,
  `description_creature` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id_creature`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `creatures`
--

INSERT INTO `creatures` (`id_creature`, `nom_creature`, `image_creature`, `description_creature`) VALUES
(1, 'Anjanath', '', 'L\'Anjanath parcourt la Forêt ancienne à la recherche d\'Aptonoth, son met favori. Ce monstre belliqueux attaque sans hésitation.'),
(2, 'Khezu', '', 'Une wyverne répugnante vivant dans les grottes et autres lieux sombres. Presque aveugle, le Khezu chasse avec son odorat et utilise des décharges électriques pour paralyser sa proie avant de bondir depuis les parois.'),
(3, 'Rathian', '', 'Wyverne femelle crachant du feu aussi appelée \"reine de la terre\". Avec ses pattes puissantes et sa queue sécrétant du poison, elle chasse principalement au sol. On la voit parfois coopérer avec le Rathalos'),
(4, 'Blangonga', '', 'A la tête des meutes de Blango, le Blangonga est reconnaissable par sa taille, ses grandes moustaches et ses énormes crocs. Il envoi la meute attaquer les intrus. Sa capacité à sauter lui confère une grande mobilité au milieu de la Crête arctique.'),
(5, 'Kirin', '', 'Un dragon ancien si rare qu\'on en sait très peu sur son mode de vie, même si tout le monde connaît la grande valeur des morceaux de Kirin. On dit qu\'il ne fait qu\'un avec la foudre et que son corps s\'enveloppe d\'électricité quand on le provoque.'),
(6, 'Lao-Shan Lung', '', 'Un dragon aux proportions extraordinaires qui n\'a été que rarement aperçu. Lorsqu\'il lui arrive de se mettre en chasse près des zones habitées, les dégâts sont indescriptibles. Son mode de vie et ses cycles d\'errance restent un mystère. La Guilde installe des remparts de protection après chacun de ses passages, mais sans grands résultats.'),
(7, 'Pukei-Pukei corail', '', 'Wyverne rapace, sous-espèce du Pukei-Pukei. Stocke l\'eau dans son corps, renforcée par les végétaux qu\'elle mange. En cas de menace, elle projette un flot puissant par la gueule ou la queue.'),
(8, 'Grand Jagras', '', 'Leader de meute des Jagras, le Grand Jagras se frotte à n\'importe quoi lorsqu\'il est en colère. SOn corps gonfle quand il avale une proie.'),
(9, 'Plesioth', '', 'Wyverne aquatique géante que l\'on peut voir près des étendues d\'eau. Là où d\'autres wyvernes se voient pousser des ailes, ce monstre a développé des nageoires lui permettant de nager. Malgré son apparence de poisson, il reste à l\'aise sur terre.'),
(10, 'Najarala', '', 'Un monstre qui étourdit sa proie grâce aux vibrations de ses écailles, puis l\'enveloppe et l\'étouffe. Si un Najarala commence à s\'enrouler autour de vous, une évasion rapide ou un meurtre éclair sont vos seules chances de survie.'),
(11, 'Bulbizarre', '', 'Bulbizarre est une créature courtaude et vaguement reptilienne qui se déplace à quatre pattes et possède un corps bleu-vert clair avec des taches bleu-vert plus foncées. Le bulbe sur son dos semble être lié à lui par une relation symbiotique.'),
(12, 'Héricendre', '', 'Héricendre ressemble à une sorte de hérisson sans piquants à ventre et pattes jaunes et à dos gris-bleu avec 4 taches rouges d\'où sortent des flammes quand il est en colère.'),
(13, 'Flobio', '', 'Le corps de Flobio est enveloppé par un film fin et collant qui lui permet de vivre hors de l\'eau. Ce Pokémon joue dans la vase sur les plages lorsque la marée est basse.'),
(14, 'Roucool', '', 'Roucool a un sens de l\'orientation extrêmement aiguisé, surtout pour rentrer chez lui. Il peut localiser son nid même s\'il a été déplacé. C\'est un Pokémon docile, et il préfère s\'enfuir en volant devant ses ennemis plutôt que de les combattre. '),
(15, 'Statitik', '', 'Statitik est semblable à un petit insecte. Il est recouvert d’une fourrure jaune, et dispose de deux yeux bleus et deux poche-accus bleus. Avec une taille de 10 cm, il partage la plus petite taille des Pokémon connus.'),
(16, 'Ouistempo', '', 'Ouistempo est un Pokémon qui ressemble à un jeune primate, au corps principalement vert avec une queue et des oreilles brunes ainsi que des membres et un museau orange. Il utilise son bâton pour taper rythmiquement les surfaces de toutes sortes d’objets.'),
(17, 'Flamiaou', '', 'Flamiaou est un Pokémon solitaire qui ne montre généralement pas ses émotions et auquel il est préférable de ne pas donner trop d\'affection.'),
(18, 'Dialga', '', 'Il peut contrôler le temps. Les mythes de Sinnoh en parlent comme d\'une divinité ancienne.'),
(19, 'Suicune', '', 'Suicune a le pouvoir de marcher sur l\'eau et de la purifier en un seul geste, d\'où son nom, le « Monarque des eaux ». Il voyage à travers le monde, à la recherche d\'eau à purifier. Les vents du Nord semblent toujours souffler plus fort en sa présence. '),
(20, 'Xerneas', '', 'La splendide ramure dont Xerneas est paré lui confère le pouvoir d\'accorder la vie éternelle.'),
(21, 'Algoule', '', 'Les algoules sont des goules qui ont mangé des cadavres pendant des années, jusqu\'à ce qu\'elles découvrent la saveur de la chair humaine et commencent à attaquer les vivants pour se repaître de leur viande tiède. On les trouve dans les cryptes et sur les champs de bataille, généralement entourées de goules.'),
(22, 'Noyeur', '', 'Il arrive qu\'une personne morte noyée revienne sous la forme d\'un monstre pour hanter les vivants. Cette créature tourmentée est guidée par la soif de tuer. Sa méthode consiste à attirer sa victime sous la surface, à la déchiqueter avec ses griffes acérées pour ensuite la dévorer, tel un biscuit mouillé. Voilà ce qu\'est un noyeur.'),
(23, 'Cocatrix', '', 'La croyance populaire prête au cocatrix, tout comme au basilic, le pouvoir de changer ses proies en pierre d\'un seul regard. Cette croyance n\'a cependant aucun fondement et le regard du cocatrix n\'est pas plus dangereux que celui d\'une oie en colère. Son bec acéré et sa longue queue sont en revanche des armes autrement plus redoutables… et bien réelles.'),
(24, 'Barghest', '', 'On raconte que les barghest sont des spectres qui se matérialisent sous forme de chiens fantomatiques et persécutent les vivants. Selon certaines histoires, ces monstres sont l\'avant-garde de la Chasse Sauvage. Selon d\'autres, ils ont un châtiment divin et incarnent la vengeance. Tous les mythes s\'accordent toutefois sur un point : les barghests sont sans pitié pour les vivants.'),
(25, 'Bloedzuiger', '', 'Le bloedzuiger est un affreux monstre des marais qui sème la terreur parmi les paysans, car il inocule des sucs digestifs dans les plaies de ceux qui sont encore en vie, puis se repaît de leurs intestins à moitié digérés.'),
(26, 'Décharneur', '', ' Les décharneurs sont rares, mais quand ils apparaissent dans une nécropole, ils en prennent le contrôle. Toutes les goules les respectent et leur doivent allégeance.'),
(27, 'Harpie', '', 'Non contente d\'être d\'une laideur repoussante, la harpie (tout comme sa cousine la chichiga) dégage une odeur pestilentielle, savoureux mélange de pourriture et d\'excréments. Même les rats, pourtant habitués à vivre parmi la matière fécale et les détritus, évitent soigneusement les nids de harpie.'),
(28, 'Draugir', '', 'Les draugir sont des âmes damnées piégées dans une coquille formée par les armes et les armures, les machines et les cadavres dévorés.'),
(29, ' Arachas', '', 'Les arachnides sont des chasseurs solitaires. Elles attendent patiemment que leur proie approche puis elles la tuent d\'un coup rapide et puissant. Il en va de même pour les arachas, énormes créatures établies dans la forêt, côté fleuve, où elles règnent en maîtres incontestés. Elles ne tolèrent d\'ailleurs aucun autre chasseur sur leur territoire, et les sorceleurs encore moins.'),
(30, 'Banshees', '', 'Les rumeurs d\'antan prétendent que les banshees sont les esprits de femmes piégées entre la vie et la mort en raison d\'expériences traumatiques. Leurs lamentations seraient l\'augure funeste d\'une mort imminente et inéluctable, bien qu\'on prétende qu\'elles ne s\'en prennent pas aux vivants eux-mêmes. La plupart du temps, elles se manifestent sous la forme de femmes à la peau blafarde, le visage fané et strié de larmes, le corps pareil à un cadavre.');

-- --------------------------------------------------------

--
-- Structure de la table `creature_espece`
--

DROP TABLE IF EXISTS `creature_espece`;
CREATE TABLE IF NOT EXISTS `creature_espece` (
  `id_creature` int(11) NOT NULL,
  `id_espece` int(11) NOT NULL,
  PRIMARY KEY (`id_creature`,`id_espece`),
  KEY `id_creature` (`id_creature`),
  KEY `id_espece` (`id_espece`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `creature_espece`
--

INSERT INTO `creature_espece` (`id_creature`, `id_espece`) VALUES
(1, 1),
(2, 2),
(3, 2),
(4, 3),
(5, 4),
(6, 4),
(7, 5),
(8, 13),
(9, 6),
(10, 7),
(18, 8),
(19, 8),
(20, 8),
(21, 9),
(23, 10),
(24, 11),
(26, 9),
(28, 11),
(29, 12);

-- --------------------------------------------------------

--
-- Structure de la table `creature_habitat`
--

DROP TABLE IF EXISTS `creature_habitat`;
CREATE TABLE IF NOT EXISTS `creature_habitat` (
  `id_creature` int(11) NOT NULL,
  `id_habitat` int(11) NOT NULL,
  PRIMARY KEY (`id_habitat`,`id_creature`),
  KEY `id_habitat` (`id_habitat`),
  KEY `id_creature` (`id_creature`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `creature_habitat`
--

INSERT INTO `creature_habitat` (`id_creature`, `id_habitat`) VALUES
(1, 1),
(8, 1),
(1, 2),
(2, 3),
(22, 3),
(3, 4),
(5, 7),
(7, 7),
(6, 8),
(9, 9),
(10, 10),
(11, 11),
(14, 12),
(15, 13),
(18, 14),
(19, 14),
(20, 14),
(17, 15),
(16, 16),
(21, 17),
(22, 18),
(23, 18),
(24, 19),
(25, 20),
(26, 21),
(28, 22),
(12, 24),
(13, 25),
(29, 26);

-- --------------------------------------------------------

--
-- Structure de la table `creature_type`
--

DROP TABLE IF EXISTS `creature_type`;
CREATE TABLE IF NOT EXISTS `creature_type` (
  `id_creature` int(11) NOT NULL,
  `id_type` int(11) NOT NULL,
  PRIMARY KEY (`id_creature`,`id_type`),
  KEY `id_creature` (`id_creature`),
  KEY `id_type` (`id_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `creature_type`
--

INSERT INTO `creature_type` (`id_creature`, `id_type`) VALUES
(1, 1),
(2, 6),
(3, 8),
(4, 5),
(5, 6),
(6, 7),
(7, 2),
(9, 2),
(11, 11),
(12, 1),
(13, 2),
(14, 12),
(15, 6),
(16, 11),
(17, 1),
(18, 14),
(19, 2),
(20, 15);

-- --------------------------------------------------------

--
-- Structure de la table `creature_univers`
--

DROP TABLE IF EXISTS `creature_univers`;
CREATE TABLE IF NOT EXISTS `creature_univers` (
  `id_creature` int(11) NOT NULL,
  `id_univers` int(11) NOT NULL,
  PRIMARY KEY (`id_univers`,`id_creature`),
  KEY `id_univers` (`id_univers`),
  KEY `id_creature` (`id_creature`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `creature_univers`
--

INSERT INTO `creature_univers` (`id_creature`, `id_univers`) VALUES
(1, 1),
(7, 1),
(8, 1),
(10, 2),
(4, 4),
(2, 5),
(3, 5),
(5, 5),
(6, 5),
(9, 5),
(11, 6),
(14, 6),
(12, 7),
(19, 7),
(13, 8),
(18, 9),
(15, 10),
(20, 11),
(17, 12),
(16, 13),
(21, 14),
(22, 14),
(23, 14),
(24, 14),
(25, 14),
(26, 14),
(27, 14),
(28, 14),
(29, 14),
(30, 14);

-- --------------------------------------------------------

--
-- Structure de la table `table_id_espece`
--

DROP TABLE IF EXISTS `table_id_espece`;
CREATE TABLE IF NOT EXISTS `table_id_espece` (
  `id_espece` int(11) NOT NULL AUTO_INCREMENT,
  `nom_espece` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id_espece`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `table_id_espece`
--

INSERT INTO `table_id_espece` (`id_espece`, `nom_espece`) VALUES
(1, 'Wyverne de Terre'),
(2, 'Wyverne Volante'),
(3, 'Bête à croc'),
(4, 'Dragon Ancien'),
(5, 'Wyverne Rapace'),
(6, 'Wyverne Aquatique'),
(7, 'Wyverne Reptile'),
(8, 'Légendaire'),
(9, 'Nécrophage'),
(10, 'Ornithosaure'),
(11, 'Spectre'),
(12, 'Insectoïde'),
(13, 'Wyverne à crocs');

-- --------------------------------------------------------

--
-- Structure de la table `table_id_habitat`
--

DROP TABLE IF EXISTS `table_id_habitat`;
CREATE TABLE IF NOT EXISTS `table_id_habitat` (
  `id_habitat` int(11) NOT NULL AUTO_INCREMENT,
  `nom_habitat` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id_habitat`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `table_id_habitat`
--

INSERT INTO `table_id_habitat` (`id_habitat`, `nom_habitat`) VALUES
(1, 'Forêt Ancienne'),
(2, 'Désert des termites'),
(3, 'Marais'),
(4, 'Steppe ancestrale'),
(5, 'Montagne Enneigée'),
(6, 'Montagne Enneigée d\'Yilufa'),
(7, 'Plateau de corail'),
(8, 'Forteresse'),
(9, 'Pics brumeux'),
(10, 'Forêt Primitive'),
(11, 'Bourg Palette'),
(12, 'Route 1'),
(13, 'Plaine Verdoyante'),
(14, 'Ultra-Dimension'),
(15, 'Lili\'i '),
(16, 'Paddoxton'),
(17, 'Cimetière de Wyzima'),
(18, 'Égouts'),
(19, 'Faubourgs'),
(20, 'Cimetière des marais'),
(21, 'Anciennes cryptes'),
(22, 'Champs de bataille'),
(24, 'Bourg Geon'),
(25, 'Bourg-en-Vol'),
(26, 'Forêt');

-- --------------------------------------------------------

--
-- Structure de la table `table_id_type`
--

DROP TABLE IF EXISTS `table_id_type`;
CREATE TABLE IF NOT EXISTS `table_id_type` (
  `id_type` int(11) NOT NULL AUTO_INCREMENT,
  `nom_type` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `table_id_type`
--

INSERT INTO `table_id_type` (`id_type`, `nom_type`) VALUES
(1, 'Feu'),
(2, 'Eau'),
(3, 'Terre'),
(4, 'Air'),
(5, 'Glace'),
(6, 'Foudre'),
(7, 'Dragon'),
(8, 'Poison'),
(9, 'Sommeil'),
(10, 'Paralysie'),
(11, 'Plante'),
(12, 'Vol'),
(13, 'Insecte'),
(14, 'Acier'),
(15, 'Fée');

-- --------------------------------------------------------

--
-- Structure de la table `univers`
--

DROP TABLE IF EXISTS `univers`;
CREATE TABLE IF NOT EXISTS `univers` (
  `id_univers` int(11) NOT NULL AUTO_INCREMENT,
  `nom_univers` text COLLATE utf8_bin NOT NULL,
  `image_univers` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id_univers`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `univers`
--

INSERT INTO `univers` (`id_univers`, `nom_univers`, `image_univers`) VALUES
(1, 'Monster Hunter World', ''),
(2, 'Monster Hunter 4', ''),
(3, 'Monster Hunter Génération', ''),
(4, 'Monster Hunter 2', ''),
(5, 'Monster Hunter', ''),
(6, 'Pokémon Génération 1', ''),
(7, 'Pokémon Génération 2', ''),
(8, 'Pokémon Génération 3', ''),
(9, 'Pokémon Génération 4', ''),
(10, 'Pokémon Génération 5', ''),
(11, 'Pokémon Génération 6', ''),
(12, 'Pokémon Génération 7', ''),
(13, 'Pokémon Génération 8', ''),
(14, 'The Witcher ', '');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `creature_espece`
--
ALTER TABLE `creature_espece`
  ADD CONSTRAINT `fk_creature_espece_id_espece` FOREIGN KEY (`id_creature`) REFERENCES `creatures` (`id_creature`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_id_creature_espece` FOREIGN KEY (`id_espece`) REFERENCES `table_id_espece` (`id_espece`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `creature_habitat`
--
ALTER TABLE `creature_habitat`
  ADD CONSTRAINT `fk_habitat_id_creature` FOREIGN KEY (`id_creature`) REFERENCES `creatures` (`id_creature`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_habitat_id_habitat` FOREIGN KEY (`id_habitat`) REFERENCES `table_id_habitat` (`id_habitat`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `creature_type`
--
ALTER TABLE `creature_type`
  ADD CONSTRAINT ` fk_id_creature` FOREIGN KEY (`id_type`) REFERENCES `table_id_type` (`id_type`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_id_creature` FOREIGN KEY (`id_creature`) REFERENCES `creatures` (`id_creature`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `creature_univers`
--
ALTER TABLE `creature_univers`
  ADD CONSTRAINT `fk_univers_id_creature` FOREIGN KEY (`id_creature`) REFERENCES `creatures` (`id_creature`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_univers_id_univers` FOREIGN KEY (`id_univers`) REFERENCES `univers` (`id_univers`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

use App\Comment;
use App\Room;
use App\Topic;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class TopicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create("fr_FR");

        /** @var User $user */
        $user = User::query()->find(1);

        for ($i = 0; $i < 5; $i++) {
            $topic = new Topic();
            $topic->name = join(" ", $faker->unique()->words(3));
            $topic->slug =  Str::of($topic->name)->slug('-');

            $topic->save();

            $rooms = [];
            for ($y = 0; $y < 5; $y++) {
                $room = new Room();
                $room->title = join(" ", $faker->unique()->words(2));
                $room->message = $faker->text;
                $room->user_id = $user->id;
                $room->topic_id = $topic->id;
                $room->save();
                $rooms[] = $room;

                $comments = [];

                for ($u = 0; $u < 5; $u++) {
                    $comment = new Comment();
                    $comment->comment = $faker->text;
                    $comment->room_id = $room->id;
                    $comment->user_id = $user->id;

                    $comment->save();

                }
//
//                $room->comments()->saveMany($comments);

            }

            $user->rooms()->saveMany($rooms);
            $user->comments()->saveMany($comments);

            $user->save();
        }
    }
}

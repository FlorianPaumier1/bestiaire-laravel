<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::prefix("forum/topics")->group(function(){
    Route::get('/{topic:slug}/room/{room}/comments', "CommentController@index")->name("app.forum.topic.room_comments");
    Route::post('/{topic:slug}/room/{room}/comments/{comment}/rank', "CommentController@rank")->name("app.forum.topic.room_comment_rank");
    Route::post('/{topic:slug}/room/{room}/comments', "CommentController@create")->name("app.forum.topic.room_comment_create");
});

<?php

use App\Role\UserRole;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/backup', 'HomeController@backup')->name('backup');

// Route::get( '/seances', 'HomeController@seances')->name('seances');
// Route::post( '/add_seances', 'HomeController@add_seances')->name('add_seances');

// Route::get( '/wall', 'WallController@index')->name('wall');
// Route::post( '/wall', 'WallController@write')->name('write');

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/contact', 'ContactController@index')->name('app.contact');
Route::post('/contact', 'ContactController@new')->name('app.contact.new');

Route::get("/search", "SearchController@search")->name("app.search");

Route::prefix("tutoriels")->group(function(){
    Route::get('/', "TutorialController@index")->name("app.tutorial.index");
    Route::get('/{tutorial:slug}', "TutorialController@show")->name("app.tutorial.show");
});

Route::prefix('forum')->group(function () {
    Route::get('/', 'ForumController@index')->name('app.forum');

    Route::prefix('topics')->group(function () {
        Route::get('/{topic:slug}', 'TopicController@index')->name('app.forum.topic');
        Route::get('/{topic:slug}/room/{room}', 'RoomController@index')->name('app.forum.topic.room');
        Route::get('/{topic:slug}/room', 'RoomController@new')->name('app.forum.topic.new.room')->middleware('auth');
        Route::post('/{topic:slug}/room', 'RoomController@create')->name('app.forum.topic.create.room')->middleware('auth');
        Route::patch('/{topic:slug}/room/{room}', 'RoomController@update')->name('app.forum.topic.update.room');
    });
});

Route::get('/is-connected', "UserController@isConnected")->name("app.user_is_connected");

Route::any("/univers/{univers}", function(\App\Univer $univers){
    return $univers->nom_univers;
})->name("app.univers.show");

Route::any("/species/{species}", function(){
    return "species";
})->name("app.species.show");

Route::any("/places/{places}", function(){
    return "places";
})->name("app.places.show");

Route::any("/types/{types}", function(){
    return "types";
})->name("app.types.show");

Route::namespace('Bestiary')->group(function () {

    Route::get('/creature/{creature}', 'CreatureController@creatureProfile')->name('bestiary.creature_profile');
    Route::get('/creature-list', 'CreatureController@viewCreaturesList')->name('bestiary.creature_list');
    Route::post('/creature-list', 'CreatureController@filterCreature')->name('bestiary.creature_filter');

    Route::get('/creature/{creature}/update', 'CreatureController@creatureUpdateForm')->name('bestiary.creature_update_form');
    Route::patch('/creature/{creature}/update', 'CreatureController@creatureUpdate')->name('bestiary.creature_update');

    Route::get('/creature-add', 'CreatureController@ViewAddCreature')->name('bestiary.add_creature');
    Route::post('/creature-add', 'CreatureController@addCreatureElements')->name('bestiary.add_creature_elements');

    Route::get('/creature/{creature}/delete', 'CreatureController@ViewDeleteCreature')->name('bestiary.creature_delete');
    Route::post('/creature/{creature}/delete', 'CreatureController@destroyCreature')->name('bestiary.creature_destroy');
});

Route::get('image/{filename}', 'FileController@displayImage')->name('image.displayImage');

Route::middleware('auth')->group(function () {

    Route::prefix('user')->group(function () {
        Route::get("/billing-portal", "UserController@billingPortal")->name("user.billing_portal");
        Route::get("/paiment-info", "UserController@paymentInfo")->name("user.payment_info");
        Route::get("/{user}", 'UserController@profile')->name('user.profile');
        Route::get("/{user}/edit", 'UserController@showFormUpdate')->name('user.edit_profile');
        Route::post("/{user}/edit", 'UserController@update');
        Route::delete("/{user}/delete", 'UserController@destroy')->name("user.destroy");
    });

    Route::namespace('Admin')->prefix('admin')->group(function () {

        Route::any("/", function () {
            return Redirect::route("admin.dashboard");
        });

        /** Login */
        Route::get('/login', 'Auth\LoginController@showLoginForm')->name('admin.login');
        Route::post('/login', 'Auth\LoginController@login');
        Route::post('/logout', 'Auth\LoginController@logout')->name('admin.logout');

        Route::middleware('check_user_role:' . UserRole::ROLE_ADMIN)->group(function () {

            Route::prefix("")->group(function () {

                Route::resource("tutorials", "TutorialController")->names([
                    'index' => 'admin.tutorials.index',
                    'show' => 'admin.tutorials.show',
                    'store' => 'admin.tutorials.store',
                    'create' => 'admin.tutorials.create',
                    'edit' => 'admin.tutorials.edit',
                    'update' => 'admin.tutorials.update',
                    'destroy' => 'admin.tutorials.destroy',
                ]);
                Route::prefix("/tutorial/{tutorial}")->group(function () {
                    Route::resource("tutorialChapter", "TutorialChapterController")->names([
                        'index' => 'admin.tutorial_chapter.index',
                        'show' => 'admin.tutorial_chapter.show',
                        'store' => 'admin.tutorial_chapter.store',
                        'create' => 'admin.tutorial_chapter.create',
                        'edit' => 'admin.tutorial_chapter.edit',
                        'update' => 'admin.tutorial_chapter.update',
                        'destroy' => 'admin.tutorial_chapter.destroy',
                    ]);
                });
            });

            Route::prefix("forum")->group(function () {
                Route::get("/", "ForumController@index")->name("admin.forum.index");
                Route::prefix("topics")->group(function () {
                    Route::get("/{topic}", "ForumController@edit")->name("admin.topics.edit");
                    Route::patch("/{topic}", "ForumController@update")->name("admin.topics.update");
                    Route::delete("/{topic}", "ForumController@delete")->name("admin.topics.delete");

                    Route::prefix("/{topic}/rooms")->group(function () {
                        Route::get("/", "RoomsController@index")->name("admin.rooms.index");
                        Route::get("/{room}", "RoomsController@edit")->name("admin.rooms.edit");
                        Route::patch("/{room}", "RoomsController@update")->name("admin.rooms.update");
                        Route::delete("/{room}", "RoomsController@delete")->name("admin.rooms.delete");

                        Route::prefix("/{room}/comments")->group(function () {
                            Route::get("/", "CommentsController@index")->name("admin.comments.index");
                            Route::get("/{comment}/show", "CommentsController@edit")->name("admin.comments.show");
                            Route::get("/{comment}", "CommentsController@edit")->name("admin.comments.edit");
                            Route::patch("/{comment}", "CommentsController@update")->name("admin.comments.update");
                            Route::delete("/{comment}", "CommentsController@delete")->name("admin.comments.delete");
                        });
                    });
                });
            });


            Route::get("/contact", "ContactController@index")->name("admin.contact.index");
            Route::patch("/contact/{contact}", "ContactController@update")->name("admin.contact.update");
            Route::delete("/contact/{contact}", "ContactController@delete")->name("admin.contact.delete");
            /** Dashboard */
            Route::get("/dashboard", 'DashboardController@dashboard')->name("admin.dashboard");
        });
    });
});


//CKeditor CKfinder
Route::any('/ckfinder/connector', '\CKSource\CKFinderBridge\Controller\CKFinderController@requestAction')
    ->name('ckfinder_connector');

Route::any('/ckfinder/browser', '\CKSource\CKFinderBridge\Controller\CKFinderController@browserAction')
    ->name('ckfinder_browser');


@extends('layouts.app')

@section('content')
    <ul>
        @foreach($topics as $topic)
            <li><a href="{{ route("app.forum.topic", ["topic" => $topic->slug]) }}">{{ $topic->name }}</a></li>
        @endforeach
    </ul>
@endsection

@extends('layouts.app')

@section('content')

    <section class="container">

    <a href="{{ route('app.forum.topic.new.room',["topic" => $slug]) }}" class="btn btn-primary">Nouveau Sujet</a>
    <ul>

        @foreach($rooms as $room)
            <li><a href="{{ route('app.forum.topic.room', ["topic" => $slug, 'room' => $room->id]) }}">{{ $room->title }}</a></li>
        @endforeach
    </ul>
       {{ $rooms->onEachSide(5)->links() }}
    </section>
@endsection

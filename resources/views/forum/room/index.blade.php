@extends('layouts.app')

@section('title')
    Commentaire
@endsection
@section('content')

    <section class="container">

        <h1>{{ $room->title }}</h1>
        <article class="row col-md-12 d-flex justify-content-between border">
            <section class="left col-md-3 border-right">
                <span class="row">{{ $room->user->name }}</span>
                <span class="row">{{ $room->user->email }}</span>
                <span class="row">{{ $room->created_at->format('d-m-Y') }}</span>
            </section>
            <section class="right col-md-9 row d-flex justify-content-between">
                <div class="col-md-6">
                    {!! html_entity_decode($room->message) !!}
                </div>
            </section>
        </article>
        <section id="comment-app"></section>

    </section>
@endsection

@extends("layouts.app")

@section("content")
    <section class="container">
        <h1>Nouveau Sujet</h1>

        <section class="container">
            <form action="{{ route("app.forum.topic.create.room", ["topic" => $topic]) }}" method="POST">
                @csrf
                <div class="row form-group">
                    <label for="room_title">Titre</label>
                    <input type="text" name="room_title" id="room_title" class="form-control" required>
                </div>
                <div>
                    <label for="room_description">Message</label>
                    <textarea rows="10"
                        type="text" name="room_description" id="room_description" class="ckeditor"></textarea>
                </div>
                <button type="submit" class="btn btn-success">Valider</button>
            </form>
        </section>
    </section>

@endsection

@section('javascripts')
    <script src="https://cdn.ckeditor.com/ckeditor5/19.1.1/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#room_description'))
            .then(editor => {
                // console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
    </script>
@endsection


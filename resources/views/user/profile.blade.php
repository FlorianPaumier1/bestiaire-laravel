@extends("layouts.app")

@section('content')
    <section class="container">

    <table class="table" style="width:50%">
        <tr>
            <th>Name:</th>
            <td>{{ $user->name }}</td>
        </tr>
        <tr>
            <th>Email:</th>
            <td>{{ $user->email }}</td>
        </tr>
        <tr>
            <th>Billing Portal:</th>
            <td><a href="{{ route("user.billing_portal") }}">Portal</a><br><a href="{{ route("user.payment_info") }}">Info Paiment</a></td>
        </tr>
    </table>

        <ul class="list-unstyled">
            <li>
                <a href="{{ route('user.edit_profile', ["user" => $user->id]) }}">
                    <button class="btn btn-primary">edit</button>
                </a>
            </li>
            <li>
                {{ Form::open(['method' => 'DELETE', 'route' => ['user.destroy', $user->id], "id" => "form-user-delete"]) }}
                {{ Form::submit('Delete', ['class' => 'btn btn-danger', 'data-id' => $user->id]) }}
                {{ Form::close() }}
            </li>
        </ul>

    </section>

    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    ...
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger" id="btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascripts')
    <script>
        deleteUser = (e) => {
            e.preventDefault()

            $("#confirm-delete").toggle()
        }

        const btnOk = document.getElementById("btn-ok");
        btnOk.addEventListener("click", () => {
            const form = document.getElementById("form-user-delete");
            form.submit();
        })

    </script>
@endsection

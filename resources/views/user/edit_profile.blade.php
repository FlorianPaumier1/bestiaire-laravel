@extends("layouts.app")

@section('content')
    <section class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{ route("user.edit_profile", ["user" => $user->id ]) }}" method="post">
            @csrf

            <table class="table" style="width:50%">
                <tr>
                    <th><label for="name">Name :</label></th>
                    <td>
                        <input type="text" id="name" name="name" class="form-control" value="{{ $user->name }}"/>
                    </td>
                </tr>
                <tr>
                    <th><label for="email">Email :</label></th>
                    <td>
                        <input type="email" id="email" name="email" class="form-control" value="{{ $user->email }}"/>
                    </td>
                </tr>
                <tr>
                    <th><label for="password">Password :</label></th>
                    <td>
                        <input type="password" id="password" name="password" class="form-control"/>
                    </td>
                </tr>
                <tr>
                    <th><label for="password_confirmation">Password Confirmation :</label></th>
                    <td>
                        <input type="password" id="password_confirmation" name="password_confirmation" class="form-control"/>
                    </td>
                </tr>
            </table>

            <button type="submit" class="btn btn-success">Modifier</button>
        </form>

            <a href="{{ route("user.profile", ["user" => $user->id]) }}" class="btn btn-primary">Retour</a>
    </section>
@endsection

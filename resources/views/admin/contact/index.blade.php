@extends("layouts.admin")

@section('content')
    <section class="container">

    <h1>Contact</h1>

    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th>Nom</th>
            <th>Subject</th>
            <th>Message</th>
            <th>Fermé ?</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
            @foreach($contacts as $contact)
                <tr>
                    <td>{{ $contact->id }}</td>
                    <td>{{ $contact->subject }}</td>
                    <td>{{ $contact->message }}</td>
                    <td>{{ $contact->closed ? "Oui" : "Non" }}</td>
                    <td>
                        <form action="{{ route("admin.contact.update", ["contact" => $contact])}}" method="post">
                            <input class="btn btn-default" type="submit" value="Valider" />
                            <input type="hidden" name="_method" value="patch" />
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </form>
                        <form action="{{ route("admin.contact.delete", ["contact" => $contact])}}" method="post">
                            <input class="btn btn-default" type="submit" value="Delete" />
                            <input type="hidden" name="_method" value="delete" />
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    </section>
@endsection

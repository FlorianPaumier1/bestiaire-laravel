@extends("layouts.admin")

@section('content')
    <section class="container">
        <h1>Tutoriels</h1>


        <section class="container">
            <a href="{{ route("admin.tutorials.create") }}" class="btn btn-primary">Ajout</a>
        </section>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Titre</th>
                <th>Univers</th>
                <th>Slug</th>
                <th>Visible ?</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($tutorials as $tutorial)
                <tr>
                    <td>{{ $tutorial->title }}</td>
                    <td>
                        {{$tutorial->univers->nom_univers }}
                    </td>
                    <td>{{ $tutorial->slug }} </td>
                    <td>{{ $tutorial->visible ? "Oui" : "Non" }}</td>
                    <td>
                        <a href="{{ route("admin.tutorials.show", ["tutorial" => $tutorial]) }}">
                            <button>Show</button>
                        </a>
                        <a href="{{ route("admin.tutorial_chapter.index", ["tutorial" => $tutorial]) }}">
                            <button>Chapitres</button>
                        </a>
                        <a href="{{ route("admin.tutorials.edit", ["tutorial" => $tutorial]) }}">
                            <button>Modifier</button>
                        </a>
                        <form action="{{ route("admin.tutorials.destroy", ["tutorial" => $tutorial]) }}" method="post">
                            <input class="btn btn-default" type="hidden" value="Delete" />
                            <button>Supprimer</button>
                            @method('delete')
                            @csrf
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        @if($tutorials["lastPage"] > 1)
            {{ $tutorials->onEachSide(5)->links() }}
        @endif
    </section>
@endsection

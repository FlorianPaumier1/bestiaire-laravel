@extends("layouts.admin")

@section("content")
    <section class="container">
        <h1>Edition</h1>

        <form method="post" action="{{ route("admin.tutorials.update", ["tutorial" => $tutorial->id]) }}" enctype="multipart/form-data">
            @csrf
            {{ method_field('PATCH') }}
            <div class="form-inline">
                <label class="form-label">
                    Titre :
                </label>
                {!! Form::text('title', $tutorial->title, ['class' => 'form-control']) !!}
            </div>
            <div class="form-inline">
                <label class="form-check-label">
                    Visible ?
                </label>
                {!! Form::checkbox('visible', '1', $tutorial->visible,  ['id' => 'visible']) !!}
            </div>
            <div class="form-inline">
                Thumbnail :
                {!! Form::file('thumbnail', ['class' => 'form-control-file']) !!}
            </div>
            <div class="form-inline">

                {!! Form::select('univer_id', $univers , $tutorial->univers->id_univers , ['class' => 'form-control']) !!}
            </div>
            {!! Form::submit('Valider', ['class' => 'btn btn-success']) !!}
        </form>

    </section>
@endsection

@extends("layouts.admin")

@section("content")

    <section class="container">
        <table>
            <tr>
                <th>Titre :</th>
                <td>{{ $tutorial->title }}</td>
            </tr>
            <tr>
                <th>Univers :</th>
                <td>{{ $tutorial->univer_id }}</td>
            </tr>
            <tr>
                <th>Slug :</th>
                <td>{{ $tutorial->slug }}</td>
            </tr>
            <tr>
                <th>Thumbnail :</th>
                <td>
                    <img src="{{ asset($tutorial->thumbnail) }}" alt="tutorial thumbnail"/>
                </td>
            </tr>
            <tr>
                <th>Visible ?</th>
                <td>{{ $tutorial->visible ? "Oui" : "Non" }}</td>
            </tr>
        </table>

        <a href="{{ route("admin.tutorials.index") }}" class="btn btn-primary">Retour</a>
    </section>
@endsection

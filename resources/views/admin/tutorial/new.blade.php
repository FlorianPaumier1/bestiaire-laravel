@extends("layouts.admin")

@section("content")
    <section class="container">
        <h1>Ajout</h1>

        <form method="post" action="{{ route("admin.tutorials.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-inline">
                <label class="form-label">
                    Titre :
                </label>
                {!! Form::text('title', $tutorial->title, ['class' => 'form-control']) !!}
            </div>
            <div class="form-inline">
                <label class="form-check-label">
                    Visible ?
                </label>
                {!! Form::checkbox('visible', '1', true,  ['id' => 'visible']) !!}
            </div>
            <div class="form-inline">
                Thumbnail :
                {!! Form::file('thumbnail', ['class' => 'form-control-file']) !!}
            </div>
            <div class="form-inline">
                Univer :
                {!! Form::select('univer_id', $univers , $tutorial->univer_id , ['class' => 'form-control']) !!}
            </div>
            {!! Form::submit('Valider', ['class' => 'btn btn-success']) !!}
        </form>

    </section>
@endsection

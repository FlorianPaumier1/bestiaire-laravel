@extends("layouts.admin")

@section('content')
    <h1>Forum</h1>


    <section class="container">

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Fermer ?</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
            @foreach($topics as $topic)
                <tr>
                    <td>{{ $topic->name }}</td>
                    <td>{{ $topic->close ? "Oui" : "Non" }}</td>
                    <td>
                        <a href="{{ route("admin.topics.edit", ["topic" => $topic]) }}">Edit</a>
                        <a href="{{ route("admin.rooms.index", ["topic" => $topic]) }}">Show Rooms</a>
                        <form action="{{ route("admin.topics.delete", ["topic" => $topic]) }}" method="post">
                            <input class="" type="submit" value="Delete" />
                            <input type="hidden" name="_method" value="delete" />
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

        {{ $topics->onEachSide(5)->links() }}
    </section>
@endsection

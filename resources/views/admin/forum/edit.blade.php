@extends("layouts.admin")

@section('content')
    <h1>Topic {{ $topic->name }}</h1>

    <section class="container">
        <form action="{{ route("admin.topic.edit", ["topic" => $topic->id]) }}" method="post">
            {{ method_field('PATCH') }}
            @csrf
            <div class="form-group">
                <label for="topic_name">Nom</label>
                <input type="text" name="topic_name" id="topic_name" class="form-control" value="{{ $topic->name }}">
            </div>
            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="topic_close" name="topic_close">
                <label class="custom-control-label" for="topic_close">Fermer ?</label>
            </div>
            <button type="submit" class="btn btn-primary mt-5">Valider</button>
        </form>
    </section>
@endsection

@extends("layouts.admin")

@section('content')
    <h1>Salon {{ $room->title }}</h1>

    <section class="container">
        <form action="{{ route("admin.rooms.edit", ["topic" => $topic, "room" => $room]) }}" method="post">
            {{ method_field('PATCH') }}
            @csrf
            <div class="form-group">
                <label for="room_title">Nom</label>
                <input type="text" name="room_title" id="room_title" class="form-control" value="{{ $room->title }}">
            </div>
            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="room_close" name="room_close" {{ $room->close ? "checked" : "" }}>
                <label class="custom-control-label" for="room_close">Fermer ?</label>
            </div>
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="room_moderate" name="room_moderate" {{ $room->moderate ? "checked" : "" }}>
                <label class="form-check-label" for="room_moderate">Visible ?</label>
            </div>
            <button type="submit" class="btn btn-primary mt-5">Valider</button>
        </form>
    </section>
@endsection

@extends("layouts.admin")

@section('content')
    <h1>Forum</h1>


    <section class="container">

        <table class="table table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Fermer ?</th>
                <th>Moderate ?</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($rooms as $room)
                <tr>
                    <td>{{ $room->title }}</td>
                    <td>{{ $room->close ? "Oui" : "Non" }}</td>
                    <td>{{ $room->moderate ? "Non" : "Oui" }}</td>
                    <td>
                        <a href="{{ route("admin.rooms.edit", ["topic" => $topic, "room" => $room]) }}">Edit</a>
                        <a href="{{ route("admin.comments.index", ["topic" => $topic, "room" => $room]) }}">Comments</a>
                        <form action="{{ route("admin.rooms.delete", ["topic" => $topic, "room" => $room]) }}" method="post">
                            <input class="" type="submit" value="Delete" />
                            <input type="hidden" name="_method" value="delete" />
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $rooms->onEachSide(5)->links() }}
    </section>
@endsection

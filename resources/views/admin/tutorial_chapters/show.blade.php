@extends("layouts.admin")

@section("content")

    <section class="container">
        <table>
            <tr>
                <th>Titre :</th>
                <td>{{ $chapter->title }}</td>
            </tr>
            <tr>
                <th>Position :</th>
                <td>{{ $chapter->position }}</td>
            </tr>
            <tr>
                <th>Description :</th>
                <td>
                    {!! $chapter->description !!}
                </td>
            </tr>
            <tr>
                <th>Content</th>
                <td>
                    {!! $chapter->content !!}
                </td>
            </tr>
        </table>

        <a href="{{ route("admin.tutorial_chapter.index", ["tutorial" => $chapter->tutorial]) }}">
            Retour
        </a>
    </section>
@endsection

@extends("layouts.admin")

@section("content")
    <section class="container">
        <h1>Ajout</h1>

        <form method="post" action="{{ route("admin.tutorial_chapter.update", ["tutorial" => $tutorial, "tutorialChapter" => $chapter]) }}" enctype="multipart/form-data">
            @csrf
            @method('patch')
            <div class="form-inline">
                <label class="form-label">
                    Titre :
                </label>
                {!! Form::text('title', $chapter->title, ['class' => 'form-control']) !!}
            </div>
            <div class="form-inline">
                <label class="form-check-label">
                    Position :
                </label>
                {!! Form::number('position', $chapter->position, ['class' => 'form-control']) !!}
            </div>
            <div class="form-inline">
                Description :
                {!! Form::textarea('description',$chapter->description, ['class' => 'form-control']) !!}
            </div>
            <div class="form-inline">
                Content :
                {!! Form::textarea('content',$chapter->content, ['class' => 'form-control']) !!}
            </div>
            {!! Form::submit('Valider', ['class' => 'btn btn-success']) !!}
        </form>

    </section>
@endsection

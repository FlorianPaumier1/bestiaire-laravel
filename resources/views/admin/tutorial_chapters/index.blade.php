@extends("layouts.admin")

@section('content')
    <section class="container">
        <h1>Chapitres</h1>


        <section class="container">
            <a href="{{ route("admin.tutorial_chapter.create", ["tutorial" => $tutorial]) }}" class="btn btn-primary">Ajout</a>
        </section>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Titre</th>
                <th>Position</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($chapters as $chapter)
                <tr>
                    <td>{{ $chapter->title }}</td>
                    <td>
                        {{ $chapter->position }}
                    </td>
                    <td>
                        <a href="{{ route("admin.tutorial_chapter.show", ["tutorial" => $chapter->tutorial, "tutorialChapter" => $chapter]) }}">
                            <button>Show</button>
                        </a>
                        <a href="{{ route("admin.tutorial_chapter.edit", ["tutorial" => $chapter->tutorial, "tutorialChapter" => $chapter]) }}">
                            <button>Modifier</button>
                        </a>
                        <a href="{{ route("admin.tutorial_chapter.destroy", ["tutorial" => $chapter->tutorial, "tutorialChapter" => $chapter]) }}">
                            <button>Supprimer</button>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        @if($chapters["lastPage"] > 1)
            {{ $chapters->onEachSide(5)->links() }}
        @endif
    </section>
@endsection

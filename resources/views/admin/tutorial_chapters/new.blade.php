@extends("layouts.admin")

@section("content")
    <section class="container">
        <h1>Ajout</h1>

        <form method="post" action="{{ route("admin.tutorial_chapter.store", ["tutorial" => $tutorial]) }}" enctype="multipart/form-data">
            @csrf
            <div class="form-inline">
                <label class="form-label">
                    Titre :
                </label>
                {!! Form::text('title', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-inline">
                <label class="form-check-label">
                    Position :
                </label>
                {!! Form::number('position', '1', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-inline">
                Description :
                {!! Form::textarea('description',null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-inline">
                Content :
                {!! Form::textarea('content',null, ['class' => 'form-control']) !!}
            </div>
            {!! Form::submit('Valider', ['class' => 'btn btn-success']) !!}
        </form>

    </section>
@endsection

@section('javascripts')
    <script>
        loadCkeditor(document.querySelectorAll("textarea"));
    </script>
@endsection

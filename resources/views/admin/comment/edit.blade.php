@extends("layouts.admin")

@section('content')
    <h1>Topic {{ $comment->name }}</h1>

    <section class="container">
        <form action="{{ route("admin.comments.edit", ["topic" => $topic, "room" => $room, "comment" => $comment]) }}" method="post">
            {{ method_field('PATCH') }}
            @csrf
            <div class="form-group">
                <label for="topic_name">Content</label>
                <textarea type="text" name="comment_content" id="comment_content" class="form-control"> {{ $comment->comment }}</textarea>
            </div>
            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="comment_moderate" name="comment_moderate" {{ $room->moderate ? "checked" : "" }}>
                <label class="custom-control-label" for="comment_moderate">Visible ?</label>
            </div>
            <div class="form-group">
                <label for="comment_rate">Rate</label>
                <input type="number" min="0" class="form-control" id="comment_rate" name="comment_rate" value="{{ $comment->rate }}">
            </div>

            <button type="submit" class="btn btn-primary mt-5">Valider</button>
        </form>
    </section>
@endsection

@extends("layouts.admin")

@section('content')
    <h1>Comment</h1>


    <section class="container">

        <table class="table table-striped">
            <thead>
            <tr>
                <th>Id</th>
                <th>User</th>
                <th>Moderate ?</th>
                <th>Rating</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($comments as $comment)
                <tr>
                    <td>{{ $comment->id }}</td>
                    <td> {{ $comment->user()->getResults()->email  }}</td>
                    <td>{{ $comment->moderate ? "Oui" : "Non" }}</td>
                    <td>{{ (int)$comment->rate }}</td>
                    <td>
                        <a href="{{ route("admin.comments.edit", ["topic" => $topic, "room" => $room, "comment" => $comment]) }}">Edit</a>
                        <form action="{{ route("admin.comments.delete", ["topic" => $topic, "room" => $room, "comment" => $comment]) }}" method="post">
                            <input class="" type="submit" value="Delete" />
                            <input type="hidden" name="_method" value="delete" />
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $comments->onEachSide(5)->links() }}
    </section>
@endsection

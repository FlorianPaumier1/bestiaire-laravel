@extends("layouts.app")

@section('content')

    <section>
        <img src="{{ asset($tutorial->thumbnail) }}" alt="image jeu">

    </section>
    <section class="row">
        <section class="p-5 col-md-2">
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a href="{{ route("app.tutorial.show", ["tutorial" => $tutorial]) }}">Intro</a>
                </li>
                @foreach($tutorial->tutorialChapters as $chapterNav)
                    <li class="nav-item">
                        <a href="{{ route("app.tutorial.show", ["tutorial" => $tutorial]) }}?chapter={{$chapterNav->id}}">{{ $chapterNav->title }}</a>
                    </li>
                @endforeach
            </ul>
        </section>
        <section class="col-md-10">
            @if($chapter)
                <div class="row justify-content-center">
                    <h2 class="d-flex justify-content-center">
                        {{ $chapter->title }}
                    </h2>
                    <section class="d-flex justify-content-center">
                        {!! $chapter->description  !!}
                    </section>
                </div>
                <div class="container">
                    {!!  $chapter->content() !!}
                </div>
            @else
                <div class="row justify-content-center">
                    <h2 class="d-flex justify-content-center">
                        {{ $tutorial->title }}
                    </h2>
                    <section class="d-flex justify-content-center">
                        {!!  $tutorial->description !!}
                    </section>
                </div>
            @endif
        </section>
    </section>
@endsection

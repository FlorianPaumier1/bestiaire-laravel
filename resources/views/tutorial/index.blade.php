@extends("layouts.app")

@section('content')


    <section class="container">

        @foreach($univers as $key => $univer)
            <section class="card">
                <div class="card-header">
                    <h1>
                        {{\App\Univer::query()->firstWhere("id_univers", "=", $key)->nom_univers }}

                    </h1>
                </div>
                <div class="card-body">
                @foreach($univer as $key => $tutorial)
                    @if($key % 3 == 0)
                        <div class="row justify-content-around">
                            @endif
                            <div class="card" style="width: 18rem;">
                                <img src="{{ asset($tutorial["thumbnail"]) }}" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $tutorial["title"] }}</h5>
                                    <div class="card-text">
                                        {!! Str::limit($tutorial["description"], 100, ' ...') !!}
                                    </div>
                                </div>
                                <div class="card-footer justify-content-center d-flex">
                                    <a href="{{ route("app.tutorial.show", ["tutorial" => $tutorial["slug"]]) }}" class="btn btn-primary">Voir le tutoriel</a>
                                </div>
                            </div>
                            @if($key % 3 == 2)
                        </div>
                    @endif
                @endforeach
                </div>
            </section>
        @endforeach
    </section>
@endsection

@extends("layouts.app")

@section("content")
    <section class="container">
        <h1>Search</h1>

        <section>
            <ul id="accordion" class="list-unstyled">
                @foreach($results as $key => $data)

                    @if (count($data) == 0)
                        @continue;
                    @endif
                    <li class="card">
                        <div class="card-header" data-toggle="collapse" data-target="#collapse-{{ $key }}" aria-expanded="false" aria-controls="collapse-{{ $key }}">
                            <h5 class="mb-0">
                                <button class="btn btn-link">
                                    {{ $key }}
                                </button>
                            </h5>
                        </div>

                        <div id="collapse-{{ $key }}" class="collapse" aria-labelledby="heading-{{ $key }}" data-parent="#accordion">
                            <div class="card-body">
                                @switch($key)
                                    @case("creatures")
                                    @include("search.cards.creature_card")
                                    @break
                                    @case("univers")
                                    @include("search.cards.univers_card")
                                    @break
                                    @case("types")
                                    @include("search.cards.type_card")
                                    @break
                                    @case("habitats")
                                    @include("search.cards.habitat_card")
                                    @break
                                    @case("especes")
                                    @include("search.cards.espece_card")
                                    @break
                                    @case("rooms")
                                    @include("search.cards.room_card")
                                    @break
                                    @case("topics")
                                    @include("search.cards.topic_card")
                                    @break
                                    @case("comments")
                                    @include("search.cards.comment_card")
                                    @break
                                    @case("tutorials")
                                    @include("search.cards.tuto_card")
                                    @break
                                    @case("tutorials Chapter")
                                    @include("search.cards.tuto_chapter_card")
                                    @break
                                    @default
                                    <span>Something went wrong, please try again</span>
                                @endswitch
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </section>
    </section>
@endsection

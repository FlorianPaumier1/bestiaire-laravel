<form action="{{ route("app.search") }}">
    <div class="inline-form">
        <div class="input-group mb-2 mr-sm-2">
            <input type="text" name="q" class="form-control" minlength="3">
            <div class="input-group-append">
                <button class="input-group-text" type="submit">Search</button>
            </div>
        </div>
    </div>
</form>

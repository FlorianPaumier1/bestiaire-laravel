@foreach($data as $value)
<div class="card">
    <div class="card-body">
        <a href="{{ route("app.species.show", ["species" => $value]) }}">{{ $value->nom_espece }}</a>
    </div>
</div>
@endforeach

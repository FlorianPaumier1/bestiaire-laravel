@foreach($data as $value)
<div class="card">
    <div class="card-header">
        <a href="{{ route("app.forum.topic.room", ["topic" => $value->topic, "room" => $value->room]) }}">Comment {{ $value->id }}</a></div>
    <div class="card-body">
        {!! Str::of($value->comment)->words(20, '...') !!}
    </div>
</div>
@endforeach

@foreach($data as $value)
<div class="card">
    <div class="card-header">
        <a href="{{ route("app.forum.topic.room", ["topic" => $value->topic, "room" => $value]) }}">Room {{ $value->title }}</a></div>
    <div class="card-body">
        {!! Str::of($value->description)->words(20, '...') !!}
    </div>
</div>
@endforeach

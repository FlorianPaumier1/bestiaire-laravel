@foreach($data as $value)
<div class="card">
    <div class="card-body">
        <a href="{{ route("app.types.show", ["types" => $value]) }}">{{ $value->nom_type }}</a>
    </div>
</div>
@endforeach

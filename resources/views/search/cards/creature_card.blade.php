@foreach($data as $value)
<div class="card">
    <div class="card-header">
        <a href="{{ route("bestiary.creature_profile", ["creature" => $value]) }}">{{ $value->nom_creature }}</a>
    </div>
    <div class="card-body">
        {{ $value->description_creature }}
    </div>
</div>
@endforeach

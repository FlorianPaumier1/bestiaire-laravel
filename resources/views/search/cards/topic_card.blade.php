@foreach($data as $value)
<div class="card">
    <div class="card-header">
        <a href="{{ route("app.forum.topic", ["topic" => $value]) }}">Topic {{ $value->name }}</a></div>
    <div class="card-body">
        {!! Str::of($value->description)->words(20, '...') !!}
    </div>
</div>
@endforeach

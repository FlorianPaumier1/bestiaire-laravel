@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h5>Modifier une créature existante</h5>
                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <form action="" method="post" name="updateForm" enctype="multipart/form-data">
                        @method("PATCH")
                        @csrf
                        <div class="update-form-card">
                            <div class="profil-name">
                                <b><label for="nom_creature">Nom de la créature : </label></b>
                                <input type="text" name="nom_creature" id="nom_creature" value="{{$creature['creature']->nom_creature}}" required>
                                <br>
                                <b><label for="espece_creature">Espece de la créature : </label></b>
                                <select name="espece">
                                    @foreach ($allEspeces as $espece)
                                    <option @isset($creature->espece->id_espece)
                                        @if($espece->id_espece == $creature['especes'][0]->id_espece) selected @endif
                                        @endisset
                                        value='{{ $espece->id_espece }}' >
                                        {{ $espece->nom_espece}}
                                        @endforeach
                                </select>
                            </div>
                            <div class="profil-icon flex-md-row justify-content-center">
                                <img src="{{ $creature['creature']->image }}" alt="creature_icon_mhw">
                                {!!  Form::file('creature_icon') !!}
                            </div>
                        </div>

                        <br>
                        <div class="types-card">
                            <div>
                                <b><label for="type_creature">Type(s) de la créature : </label></b>
                            </div>
                            <div class="types-card-checkbox type">
                                @foreach ($allTypes as $type)
                                <label>
                                    <input type="checkbox" name="types[]" @if (in_array($type->id_type, $creature['creature']->types->pluck('id_type')->all())) checked @endif
                                    value="{{ $type->id_type }}">
                                    <span class="">{{ $type->nom_type}}</span>
                                </label>
                                @endforeach
                            </div>
                        </div>
                        <br>
                        <hr>
                        <br>
                        <div class="types-card">
                            <div>
                                <b><label for="habitats_creature">Habitat(s) de la créature : </label></b>
                            </div>
                            <div class="types-card-checkbox">
                                @foreach ($allHabitat as $habitat)
                                <label>
                                    <input type="checkbox" name="habitats[]"
                                        @if (in_array($habitat->id_habitat, $creature['creature']->habitats->pluck('id_habitat')->all())) checked @endif
                                    value="{{ $habitat->id_habitat}}">
                                    <span>{{ $habitat->nom_habitat}}</span>
                                </label>
                                @endforeach
                            </div>
                        </div>
                        <br>
                        <hr>
                        <br>
                        <div class="types-card">
                            <div>
                                <b><label for="univers_creature">Univer(s) de la créature : </label></b>
                            </div>
                            <div class="types-card-checkbox">
                                @foreach ($allUnivers as $univers)
                                <label>
                                    <input type="checkbox" name="univers[]"
                                        @if (in_array($univers->id_univers, $creature['creature']->univers->pluck('id_univers')->all())) checked @endif
                                    value="{{ $univers->id_univers}}">
                                    <span>{{ $univers->nom_univers}}</span>
                                </label>
                                @endforeach
                            </div>
                        </div>
                        <br>
                        <input type=submit value=Sauvegarder>
                    </form>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
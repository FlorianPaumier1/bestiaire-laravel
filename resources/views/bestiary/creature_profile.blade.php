@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header header-profil">
                    <h5>Profil Créature</h5>
                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <div class="card-profil">
                        <div class="profil-info">
                            <h3>
                                {{$creature->nom_creature}}
                            </h3>
                            <p>
                                @foreach ($especes as $espece)
                                {{ $espece->nom_espece }}
                                @endforeach <br>
                            </p>
                            <div class="profil-subinfo">
                                <div>
                                    <h5> Habitat(s) </h5>
                                    <p>
                                        @foreach ($habitats as $habitat)
                                        {{ $habitat->nom_habitat}} <br>
                                        @endforeach <br>
                                    </p>
                                </div>
                                <div>
                                    <h5> Type(s) </h5>
                                    <p>
                                        @foreach ($types as $type)
                                        {{ $type->nom_type}} <br>
                                        @endforeach <br>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="profil-icon">
                            <img src="{{$creature->image}}" alt="creature_icon_mhw">
                        </div>
                    </div>

                    <p>{{$creature->description_creature}}</p>

                    <div>
                        <h5>Est présent dans les univers suivants : </h5>
                        <p>
                            @foreach ($univers as $univer)
                            - {{ $univer->nom_univers}} <br>
                            @endforeach
                        </p>
                    </div>
                </div>
            </div>
            <ul class="btn-profil card-footer">
                <li> <button type="submit"> <a href="{{ route('bestiary.creature_delete', ['creature' => $creature]) }}">Supprimer</a> </li>
                <li>|</li>
                <li><a href=" {{ route('bestiary.creature_update_form', ['creature' => $creature]) }}">Modifier</a></li>
                <li>|</li>
                <li><a href="{{ route('bestiary.creature_list') }}">Revenir à la liste</a></li>
            </ul>
        </div>
    </div>
</div>
@endsection

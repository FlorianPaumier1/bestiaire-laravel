@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <aside class="filter-side col-md-3">
            <br>
            <form action="" method="post">
                @csrf
                <div>
                    <!-- <input type="submit" name="nameSorting" value="Trier par Nom"> -->
                    <hr>
                    <div>
                        <label for="especeSelect"><b>Filtrer par Espèce</b></label>
                        <br>
                        <select name="especeSelect" id="especeSelect">
                            <option value="" selected>-- Choisir une espèce --</option>
                            @foreach ($allEspeces as $espece)
                            <option value='{{ $espece->id_espece }}'>
                                {{$espece->nom_espece}}
                                @endforeach
                            </option>
                        </select>
                    </div>
                    <hr>
                    <div>
                        <label for="typeSelect"><b>Filtrer par Type</b></label>
                        <br>
                        <select name="typeSelect" id="typeSelect">
                            <option value="" selected>-- Choisir un type --</option>
                            @foreach ($allTypes as $type)
                            <option value='{{ $type->id_type }}'>
                                {{$type->nom_type}}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <hr>
                    <div>
                        <label for="habitatSelect"><b>Filtrer par Habitat</b></label>
                        <br>
                        <select name="habitatSelect" id="habitatSelect">
                            <option value="" selected>-- Choisir un habitat --</option>
                            @foreach ($allHabitat as $habitat)
                            <option value='{{ $habitat->id_habitat }}'>
                                {{$habitat->nom_habitat}}
                                @endforeach
                            </option>
                        </select>
                    </div>
                    <hr>
                    <div>
                        <label for="universSelect"><b>Filtrer par Univers</b></label>
                        <br>
                        <select name="universSelect" id="universSelect">
                            <option value="" selected>-- Choisir un univers --</option>
                            @foreach ($allUnivers as $univers)
                            <option value='{{ $univers->id_univers }}'>
                                {{$univers->nom_univers}}
                                @endforeach
                            </option>
                        </select>
                    </div>
                    <br>
                    <input type="submit" name="filterSubmit" value="Appliquer">
            </form>
        </aside>
        <div class="col-md-9">
            <div class="card listing-display">

                <div>
                    <div class="card-header">
                        <br>
                        <ul class="list-title-add">
                            <li>
                                <h5>Liste Créatures</h5>
                            </li>
                            <li>
                                <a href="{{ route("bestiary.add_creature") }}">Ajouter une créature</a>
                            </li>
                        </ul>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                        @endif
                        <h1>Liste des créatures</h1>
                        <br>
                        <div class="listing-bloc">
                            @foreach ($creatureList as $creature)
                            <ul>
                                <li>
                                    <img src="{{ $creature["entity"]->image }}" alt="creature_icon" class="monster-icon-list">
                                    <br>
                                    <h5><a href="{{ route('bestiary.creature_profile', ['creature' => $creature['entity']]) }}">{{$creature['entity']->nom_creature}}</a></h5>
                                </li>
                            </ul>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header header-profil">
                    <h5>Supprimer une créature</h5>
                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <h3>Voulez-vous vraiment supprimer cette créature ?</h3>

                </div>
            </div>
            <form action="" method="post">
                @csrf
                <ul class="btn-profil card-footer">
                    <li><input type="submit" name="defDeleteBtn" value="Supprimer définitivement" class="btn-delete-def"></li>
                    <li>|</li>
                    <li><a href="{{ route('bestiary.creature_list') }}">Revenir à la liste </a></li>
                </ul>
            </form>

        </div>
    </div>
</div>
@endsection
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <ul class="add-list-ul">
                        <li>
                            <h5>Ajouter une Créature</h5>
                        </li>
                        <li><a href="{{ route('bestiary.creature_list') }}">Revenir à la liste</a></li>

                    </ul>
                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <form action="" method="POST" name="addForm">
                        @csrf
                        <b><label for="nom_creature">Nom de la créature : </label></b>
                        <input type="text" name="nom_creature">
                        <br>
                        <b><label for="nom_espece"> Espece </label></b>
                        <select name="espece">
                            @foreach ($allEspeces as $espece)
                            <option @isset($creature->espece->id_espece)
                                @if($espece->id_espece == $creature['especes'][0]->id_espece) selected @endif
                                @endisset
                                value='{{ $espece->id_espece }}' >
                                {{ $espece->nom_espece}}
                                @endforeach
                        </select>
                        <div class="profil-icon flex-md-row justify-content-center">
                            {!!  Form::file('creature_icon') !!}
                        </div>
                        <br>
                        <div class="types-card">
                            <div>
                                <b><label for="type">Type(s) de la créature : </label></b>
                            </div>
                            <div class="types-card-checkbox type">
                                @foreach ($allTypes as $type)
                                <label>
                                    <input type="checkbox" name="types[]" @if (in_array($type->id_type, $creature['creature']->types->pluck('id_type')->all())) checked @endif
                                    value="{{ $type->id_type }}">
                                    <div>{{ $type->nom_type}}</div>
                                </label>
                                @endforeach
                            </div>
                            <br>
                            <label for="addTypeLabel">Ajouter un type</label>
                            <input type="text" name="typeName" id="typeName">
                            <button type="submit" name="addTypeSubmit" value="addType">Valider</button>
                        </div>
                        <br>
                        <hr>
                        <br>
                        <div class="types-card">
                            <div>
                                <b><label for="habitats_creature">Habitat(s) de la créature : </label></b>
                            </div>
                            <div class="types-card-checkbox">
                                @foreach ($allHabitat as $habitat)
                                <label>
                                    <input type="checkbox" name="habitats[]" @if (in_array($habitat->id_habitat, $creature['creature']->habitats->pluck('id_habitat')->all())) checked @endif
                                    value="{{ $habitat->id_habitat}}">
                                    <div>{{ $habitat->nom_habitat}}</div>
                                </label>
                                @endforeach
                            </div>
                            <br>
                            <label for="addHabitatLabel">Ajouter un habitat</label>
                            <input type="text" name="habitatName" id="habitatName">
                            <button type="submit" name="addHabitatSubmit" value="addHabitat">Valider</button>
                        </div>
                        <br>
                        <hr>
                        <br>
                        <div class="types-card">
                            <div>
                                <b><label for="univers_creature">Univer(s) de la créature : </label></b>
                            </div>
                            <div class="types-card-checkbox">
                                @foreach ($allUnivers as $univers)
                                <label>
                                    <input type="checkbox" name="univers[]" @if (in_array($univers->id_univers, $creature['creature']->univers->pluck('id_univers')->all())) checked @endif
                                    value="{{ $univers->id_univers}}">
                                    <div>{{ $univers->nom_univers}}</div>
                                </label>
                                @endforeach
                            </div>
                            <br>
                            <label for="addUniversLabel">Ajouter un univers</label>
                            <input type="text" name="universName" id="universName">
                            <button type="submit" name="addUniversSubmit" value="addUnivers">Valider</button>
                        </div>
                        <br>
                        <b><label for="description">Description</label></b>
                        <textarea name="description" rows="5" cols="90"> </textarea>
                        <br><br>
                        <input type="submit" name="addCreatureSubmit" id="addCreatureSubmit" value="Ajouter la créature">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

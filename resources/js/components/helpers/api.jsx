export async function loadComment(url) {
    return await fetch( url)
        .then((res) => {
            return res.json();
        }).catch(error => {
        console.log(error)
    })
}

export async function rateComment(url, body) {
    let myHeaders = new Headers();
    myHeaders.append('Accept', 'application/json')
    myHeaders.append('Content-Type', 'application/json')
    return await fetch(url, {
        method: 'POST',
        headers: myHeaders,
        body: body
    }).then(res => {
        return res.json()
    }).catch(error => {
        console.log(error)
    })
}

export async function createComment(url, body) {
    let myHeaders = new Headers();
    myHeaders.append('Accept', 'application/json')
    myHeaders.append('Content-Type', 'application/json')
    return await fetch(url, {
        method: 'POST',
        headers: myHeaders,
        body: body
    }).then(res => {
        return res.json()
    }).catch(error => {
        console.log(error)
    })
}

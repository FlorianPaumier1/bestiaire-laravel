import React, {Component} from 'react'
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {createComment} from "../helpers/api";


export default class FormComment extends Component {

    constructor(props) {
        super(props);

        this.state = {
            editor: null,
            user: props.user,
            addComments: props.addComment
        }
    }

    async handleForm(e){
        e.preventDefault();

        const res = await createComment(
            "/api" + window.location.pathname + "/comments",
            JSON.stringify({
                comment: this.state.editor.getData(),
                user: this.state.user
            }))

        console.log(res)
        this.state.addComments({
            comment : res.comment,
            user: res.comment.user
        });
    }

    render() {
        return (
            <section>
                <form action="" onSubmit={this.handleForm.bind(this)} method={"post"}>
                    <label htmlFor="comment">Votre commentaire :</label>
                    <CKEditor
                        editor={ ClassicEditor }
                        data=""
                        onInit={ editor => {
                            // You can store the "editor" and use when it is needed.
                            this.setState({editor: editor})
                            // console.log( 'Editor is ready to use!', editor );
                        } }
                        onChange={ ( event, editor ) => {
                            const data = editor.getData();
                            // console.log( { event, editor, data } );
                        } }
                        onBlur={ ( event, editor ) => {
                            // console.log( 'Blur.', editor );
                        } }
                        onFocus={ ( event, editor ) => {
                            // console.log( 'Focus.', editor );
                        } }
                    />
                    <button className="btn btn-primary">Valider</button>
                </form>
            </section>
        )
    }
}

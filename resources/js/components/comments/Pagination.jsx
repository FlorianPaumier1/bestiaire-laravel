import React, {Component} from "react";

const Pagination = ({url, last_page, current_page}) => (
    <section className="container">
        <nav aria-label="Page navigation example">
            <ul className="pagination">
                <li className={"page-item " + (current_page - 1 <= 0 ? "disabled" : "")}><a className="page-link" href={url + "?page=" + (current_page - 1)}>Previous</a></li>
                {[...Array(last_page)].map((x, i) =>
                    <li className={"page-item " + (current_page === (i+1) ? "active" : "")} key={i}><a className="page-link" href={url + "?page=" + (i+1)}>{(i+1)}</a></li>
                )}
                <li className={"page-item " + (current_page + 1 > last_page ? "disable" : "")}><a className="page-link" href={url + "?page=" + (current_page + 1)}>Next</a></li>
            </ul>
        </nav>
    </section>
)

export default Pagination;

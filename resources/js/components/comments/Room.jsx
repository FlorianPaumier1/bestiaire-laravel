import React, {Component} from "react";
import ReactDOM from 'react-dom';
import Comment from "./Comment";
import {loadComment} from "../helpers/api";
import FormComment from "./FormComment";
import Pagination from "./Pagination";


export default class Room extends Component {

    constructor(props) {
        super(props);

        this.url = "/api" + window.location.pathname;

        this.state = {
            loading: true,
            comments: [],
            page: 1,
            message: null,
            isConnected: false
        }
    }

    async componentDidMount() {
        let res = await loadComment(this.url + "/comments")
        console.log(res)
        this.setState({
            comments: res.data,
            loading: false,
            current_page: res.current_page,
            last_page: res.last_page,
            nav_url: res.path
        })

        fetch(window.location.origin + "/is-connected").then(res => {
            return res.json();
        }).then(res => {
            this.setState({isConnected: res.connected, user: res.user})
        }).catch(error => {
            console.log(error)
        })
    }

    addComments(data) {
        console.log(data);
        this.state.comments.push(data.comment);
        this.state.user = data.user;
    }


    render() {
        return (
            <section>
                {this.state.message && (
                    <div className="alert">
                        {this.state.message}
                    </div>
                )}
                {this.state.loading && (
                    <h2>Chargement</h2>
                )}
                {this.state.comments && this.state.comments.map((comment, key) =>
                    <Comment comment={comment} key={key}/>
                )}
                { this.state.last_page > 1 && (
                    <Pagination url={this.state.nav_url} last_page={this.state.last_page} current_page={this.state.current_page}/>
                )}
                {this.state.isConnected && (
                    <FormComment user={this.state.user} addComment={this.addComments.bind(this)}/>
                )}
            </section>
        );
    }
}

if (document.getElementById('comment-app')) {
    ReactDOM.render(<Room/>, document.getElementById('comment-app'))
}


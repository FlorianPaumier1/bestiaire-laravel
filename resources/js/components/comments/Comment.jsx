import React, {Component} from 'react';
import {rateComment} from "../helpers/api";
import moment from 'moment';
import decodeHtml from "../helpers/string";
import ReactHtmlParser from 'react-html-parser'

export default class Comment extends Component {


    constructor(props) {
        super(props);

        console.log(props)
        this.state = {
            id: props.comment.id,
            comment: props.comment,
            user: {
                name: props.comment.name,
                email: props.comment.email
            },
            message: ""
        }
    }

    async rate(value) {
        let res = await rateComment(
            "/api" + window.location.pathname + "/comments/" + this.state.id + "/rank"
            ,JSON.stringify({
                value: value
            })
        )

        console.log(res)
        this.setState({
            message: res.message,
            comment: res.comment
        })
    }

    render() {
        return (
            <article className={"row col-md-12 d-flex justify-content-between border"}>
                <section className="left col-md-3 border-right">
                    <span className="row">{this.state.user.name}</span>
                    <span className="row">{this.state.user.email}</span>
                    <span className="row">{moment(this.state.comment.created_at).format("D-MM-Y")}</span>
                </section>
                <section className="right col-md-9 row d-flex justify-content-between">
                    <div className="col-md-6">
                        {ReactHtmlParser(decodeHtml(this.state.comment.comment))}
                    </div>
                    <div className="col-md-3">
                        <span className={"center"}>{this.state.comment.rate}</span>
                        <div>
                            <button className={"btn btn-primary"} onClick={() => this.rate(1)}>
                                +1
                            </button>
                            <button className={"btn btn-primary"} onClick={() => this.rate(-1)}>-1
                            </button>
                        </div>

                    </div>
                </section>
            </article>
        )
    }
}
